package digcard_wifi.app.com;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity implements OnClickListener{
	
	TextView Forgot,autoLoginText;
	EditText Name,Password;
	Button login;
	boolean connection=false;
	LoginTask loginTask ;
	String user;
	String password;
	InputStream is=null;
	String result=null;
	CheckBox autoLogin;
	int autoValue=0;
	int backgroundLogin;
	int changeTab=0;
	
	private RelativeLayout loginLayout;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
				
		setContentView(R.layout.login);
		
		
		
		/*SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("number", 3); 
		//prefEditor.putInt("out", 0);
		prefEditor.commit(); */
		
		//backgroundLogin=0;
		
		File cache = this.getCacheDir();
		
		

		
		loginLayout=(RelativeLayout) findViewById(R.id.login_layout);
		
		Name=(EditText) findViewById(R.id.user_name);
		Password=(EditText) findViewById(R.id.password);
		Forgot=(TextView) findViewById(R.id.forgot);
		login=(Button) findViewById(R.id.login);
		autoLogin=(CheckBox) findViewById(R.id.auto_login);
		autoLoginText=(TextView) findViewById(R.id.auto_login_text);
		//System.out.println("window oncreate final");
		login.setOnClickListener(this);
		
		autoLoginText.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
				int auto=loginSettings.getInt("auto", 0);
				
				
				SharedPreferences settings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
                SharedPreferences.Editor prefEditor = settings.edit();
				
                if (autoLogin.isChecked()) {
					
					//prefEditor.putInt("auto", 0);
					autoLogin.setChecked(false);
					
					autoValue=0;
					
				}else{
					
					//prefEditor.putInt("auto", 1);
					autoLogin.setChecked(true);
					
					autoValue=1;
				}
				
				prefEditor.commit();
			}
		});
		
		autoLogin.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
				int auto=loginSettings.getInt("auto", 0);
				
				
				SharedPreferences settings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
                SharedPreferences.Editor prefEditor = settings.edit();
				
                if (autoLogin.isChecked()) {
					
					//prefEditor.putInt("auto", 1);
					
					autoValue=1;
					
				}else{
					
					//prefEditor.putInt("auto", 0);
					
					autoValue=0;
				}
				
				prefEditor.commit();
			}
		});
		
		Forgot.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				SharedPreferences autoSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        	SharedPreferences.Editor prefAutoEditor = autoSettings.edit();
	        	prefAutoEditor.putInt("dntReset", 1);
	        	prefAutoEditor.commit();
				
				Intent intnt = new Intent(Login.this, Forgot.class);
				startActivity(intnt);
				
			}
		});
	}

	
	public void onStart(){
		super.onStart();
		
		loginLayout.requestFocus();
		
	}
	
	
	
	/*public void onStart(){
		super.onStart();
		
		System.out.println("onStart");
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		String auto_user_id= loginSettings.getString("auto_user_id", null);
		backgroundLogin=loginSettings.getInt("number", 3);
		
		System.out.println("backgroundLogin "+backgroundLogin);
		if(auto_user_id!=null&&backgroundLogin!=5&&backgroundLogin==1){*/
			
			/*SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = checkSettings.edit();  
			prefEditor.putInt("number", 3); 
			//prefEditor.putInt("out", 0);
			prefEditor.commit(); */
			
			/*changeTab=1;
			
			System.out.println("inside autoLoginTabChange");*/
			
			/*MainActivity parent;
	    	parent =(MainActivity) Login.this.getParent();
	    	parent.autoLoginTabChange();
	    	*/
	    	
	    	
	/*	}else{
			
			
			changeTab=0;
			SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = checkSettings.edit();  
			prefEditor.putInt("number", 3); 
			//prefEditor.putInt("out", 0);
			prefEditor.commit(); 
			
		}
	}*/
	
	/*public void onStop(){
		super.onStop();
		//backgroundLogin=0;
		
		System.out.println("onStop");
		
		SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = checkSettings.edit();  
		prefEditor.putInt("number", 3); 
		//prefEditor.putInt("out", 0);
		prefEditor.commit(); 
		
	}*/
	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		user=Name.getText().toString().trim();
		
		password=Password.getText().toString().trim();
		
		if(v == login){
			if(TextUtils.isEmpty(user)){
				
				/*Toast tost=Toast.makeText(getApplication(), "Enter User Name", Toast.LENGTH_SHORT);
					  tost.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
					  tost.show();*/
				alertFunction("Please Enter Username and Password!",0);
				
				
			}else if(TextUtils.isEmpty(password)){
				
				/*Toast tost=Toast.makeText(getApplication(), "Enter password", Toast.LENGTH_SHORT);
					  tost.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
					  tost.show();*/
				alertFunction("Please Enter Username and Password!",0);
			}else {
				
				if(connection){
					
					//postLoginDetails(User, Pass);
					
					loginTask = new LoginTask(this);
					//loginTask.execute(new String[] {"http://dev-fsit.com/iphone/login.php"});
					//loginTask.execute(new String[] {"http://dev-fsit.com/iphone/digcard/login.php"});
					//loginTask.execute(new String[] {"http://192.168.1.187/android/login.php"});
					loginTask.execute(new String[] {"http://digcardapp.com/app/login.php"});
					
					bumpDisable();
					
				}else{
					alertFunction("No Intenet Connection",0);
					//Toast.makeText(Login.this, "No network connection available", Toast.LENGTH_LONG).show();
				}
				
				
			}
			
			
		}
	}
	
	
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		
		switch(item.getItemId()){
			
			case R.id.menu_exit:
				
				MainActivity parent;
    	    	parent =(MainActivity) Login.this.getParent();
    	    	parent.exitApplication();
				break;
				
			
		}
		return super.onMenuItemSelected(featureId, item);
		
		
	}
	
	
	public void bumpDisable(){
		
		int value=1;
		MainActivity parent;
        parent =(MainActivity) Login.this.getParent();
        parent.manipulateBump(value);
	}
	
	
	public void bumpEnable(){
		
		int value=0;
		MainActivity parent;
        parent =(MainActivity) Login.this.getParent();
        parent.manipulateBump(value);
	}
	
	
	private class LoginTask extends AsyncTask<String, Void, String>{
		
		private Context mContext;
        private ProgressDialog loadingDialog;

		public LoginTask(Context context) {
			// TODO Auto-generated constructor stub
			 mContext = context;
	            loadingDialog = new ProgressDialog(mContext);
	           // loadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	            loadingDialog.setMessage("Login..");
	            loadingDialog.setCancelable(false);
	           // loadingDialog.setMax(100);
	            loadingDialog.show();
			
		}


		@Override
		protected String doInBackground(String... urls) {
			// TODO Auto-generated method stub
			
			user=Name.getText().toString();
			
			password=Password.getText().toString();
			
			for (String url : urls) {
				
				try {	
				
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(url);
					
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		            nameValuePairs.add(new BasicNameValuePair("username", user));
		            nameValuePairs.add(new BasicNameValuePair("password", password));
		            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		            
		            HttpResponse execute = httpclient.execute(httppost);
					//System.out.println("http post response");
					HttpEntity rentity     = execute.getEntity();
					is = rentity.getContent();
					
					
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				
				try{
					
					String line = "";
				     StringBuilder total = new StringBuilder();
				     // Wrap a BufferedReader around the InputStream
				     BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"),8);
				     // Read response until the end
				     try {
				      while ((line = rd.readLine()) != null) { 
				    	 
				        total.append(line); 
				      }
				     } catch (IOException e) {
				      e.printStackTrace();
				     }				    				     									
		        	is.close();
		        	result=total.toString();
		        	
				}catch(Exception e){
					Log.e("log_tag", "Error converting result "+e.toString());
				}
				
			}
			
			return result;
		}
		
		
		protected void onProgressUpdate(Integer... values)
        {
			 loadingDialog.show();
			
        }
		
		
		@Override
		protected void onPostExecute(String list) {
			
			
			bumpEnable();
			
			String data="";
			
			try {
				loadingDialog.dismiss();
				loadingDialog = null;
		    } catch (Exception e) {
		        // nothing
		    }
			
			try{
				
				 data=list.trim();
				
			}catch(Exception e){
				
				
			}
			 
			 
			 if(data.toString().equalsIgnoreCase("false")){
	            	Log.w("SENCIDE", "FALSE");
	                //Toast.makeText(getApplication(), "Incorrect Username or Password", Toast.LENGTH_SHORT).show();           
	            	alertFunction("Incorrect Username or Password",1);
	            
	            }else{
	            	
	            	
	            	//backgroundLogin=1;
	            	
	            	SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	            	String checkFirstTime= checkSettings.getString("firstTime", null);
	             
	            	SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	                
	                SharedPreferences.Editor prefEditor = loginSettings.edit();  
	                prefEditor.putString("user_name", user);  
	                prefEditor.putString("password", password); 
	                prefEditor.putString("user_id", data);
	                prefEditor.putInt("fetch", 1);
	                prefEditor.putString("firstTime", "no");
	                prefEditor.putInt("login", 1);
	                prefEditor.putInt("loginSearch", 1);
	                prefEditor.putInt("auto", autoValue);
	                
	                if(autoValue==1){
	                	
	                	prefEditor.putString("auto_user_name", user);  
		                prefEditor.putString("auto_password", password); 
		                prefEditor.putString("auto_user_id", data);
	                	
	                }
	                
	                if(checkFirstTime==null){
	                	
	                	prefEditor.putInt("checkFirstTime", 0);
	                	
	                }else{
	                	
	                	prefEditor.putInt("checkFirstTime", 1);
	                	
	                }
	                prefEditor.putInt("out", 0);
	                prefEditor.commit();
	                
	                Name.setText("");
	    			
	    			Password.setText("");
	    			
	    			autoLogin.setChecked(false);
	                
	                SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        	    int bumpActive= deleteSettings.getInt("bump", 0);
	        	    
	        	    /*if(bumpActive == 1){
	        	    	
	        	    	MainActivity parent;
	        	    	parent =(MainActivity) Login.this.getParent();
	        	    	parent.bumpServiceCancel(bumpActive);
	        	    	
	        	    }
	                	
	                Intent intent= new Intent(Login.this, MainActivity.class);
	                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	                startActivity(intent);*/
	        	    
	        	    
	        	    MainActivity parent;
        	    	parent =(MainActivity) Login.this.getParent();
        	    	parent.changeTab();
	            	
	            }
	 
	        
			
		}
		
	}

	/*private void postLoginDetails(String user, String pass) {
		// TODO Auto-generated method stub
		
		HttpClient httpclient = new DefaultHttpClient();
		//HttpPost httppost = new HttpPost("http://192.168.1.187/android/login_android.php");
		HttpPost httppost = new HttpPost("http://dev-fsit.com/iphone/login.php");
		try {
            // Add user name and password
        
          
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("username", user));
            nameValuePairs.add(new BasicNameValuePair("password", pass));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            
            
            
            
 
            // Execute HTTP Post Request
            Log.w("SENCIDE", "Execute HTTP Post Request");
            HttpResponse response = httpclient.execute(httppost);
             
            String str = inputStreamToString(response.getEntity().getContent()).toString();
            Log.w("SENCIDE", str);
            String data=str.trim();
             
            if(data.toString().equalsIgnoreCase("false"))
            {
            	Log.w("SENCIDE", "FALSE");
                Toast.makeText(getApplication(), "Login Failed", Toast.LENGTH_SHORT).show();           
            
             //intnt.putExtra("user_name", user);
            // intnt.putExtra("password", pass);
             
             
             
            // switchTabInActivity(1);
             
            }else{
             
            	SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
                
                SharedPreferences.Editor prefEditor = loginSettings.edit();  
                prefEditor.putString("user_name", user);  
                prefEditor.putString("password", pass); 
                prefEditor.putString("user_id", data);
                prefEditor.putInt("fetch", 1);
                prefEditor.putInt("out", 0);
                prefEditor.commit();  
                	
                Intent intent= new Intent(Login.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            	
            }
 
        } catch (ClientProtocolException e) {
         e.printStackTrace();
        } catch (IOException e) {
         e.printStackTrace();
        }
    } 
	
	
	 
	
	
   
    private StringBuilder inputStreamToString(InputStream is) {
     String line = "";
     StringBuilder total = new StringBuilder();
     // Wrap a BufferedReader around the InputStream
     BufferedReader rd = new BufferedReader(new InputStreamReader(is));
     // Read response until the end
     try {
      while ((line = rd.readLine()) != null) { 
    	  System.out.println("total");
        total.append(line); 
      }
     } catch (IOException e) {
      e.printStackTrace();
     }
     // Return full string
     return total;
    }*/
    
    
    public void onResume(){
		super.onResume();
		
		MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	parent.manipulateTitle("Login");
		
		
		/*if(changeTab==1){
			
			MainActivity parent;
	    	parent =(MainActivity) Login.this.getParent();
	    	parent.autoLoginTabChange();
	    	
		}else{*/
			
			
			SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = loginSettings.edit();  
			prefEditor.putInt("number", 3); 
			//prefEditor.putInt("out", 0);
			prefEditor.commit(); 
			
		//}
		
		
		
	 connection=haveNetworkConnection(); 
	 
	 if(connection ==false){
		 alertFunction("No Internet Connection",0);
		 //Toast.makeText(Login.this, "No network connection available", Toast.LENGTH_LONG).show();
	 }
		
	 
	 autoLoginCheck();
	 
	// bumpServiceStart();
  }
    
    public void autoLoginCheck(){
    	
    	SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
    	int auto=loginSettings.getInt("auto", 0);
    	String userName=loginSettings.getString("auto_user_name", null);
    	String passWord = loginSettings.getString("auto_password", null);
    	
    	if(auto==1){
    		
        	SharedPreferences settings = getSharedPreferences("Myprofile", MODE_PRIVATE);
        	SharedPreferences.Editor prefEditor = settings.edit();
        	prefEditor.putInt("auto", 0);
        	prefEditor.commit();
    		
    		
    	}else{
    		
    		
    	}
    	
    }
    
  /*
    private void bumpServiceStart(){
		
		SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        int recieverActive=deleteSettings.getInt("reciever", 0);
        
        if(bumpActive==0){
        	
        	System.out.println("going to start bump service");
        	
        	MainActivity parent;
        	parent =(MainActivity) this.getParent();
        	parent.bumpServiceStart();
        	
        }	
	}
    */
  
  private boolean haveNetworkConnection() {
	    boolean haveConnectedWifi = false;
	    boolean haveConnectedMobile = false;

	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	            if (ni.isConnected())
	                haveConnectedWifi = true;
	        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	            if (ni.isConnected())
	                haveConnectedMobile = true;
	    }
	    return haveConnectedWifi || haveConnectedMobile;
	}
	
  
  public void onDestroy(){
	  super.onDestroy();
  }
  
  @Override
  public void onBackPressed() {
  	
  	
	  SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
	    int bumpActive= deleteSettings.getInt("bump", 0);
	    final int auto= deleteSettings.getInt("auto", 0);
	  /*  if(bumpActive == 1){
	    	
	    	MainActivity parent;
	    	parent =(MainActivity) this.getParent();
	    	parent.bumpServiceCancel(bumpActive);
	    	
	    }
  	
  	*/
  	
  	//super.onBackPressed();
  	
  	AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
	builder.setTitle("Exit");
	builder.setCancelable(false);
	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

	  public void onClick(DialogInterface dialog, int arg1) {
	  // do something when the OK button is clicked
		  
		  dialog.dismiss();
		  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		  	SharedPreferences.Editor prefEditor = loginSettings.edit(); 
		  	
		  	//if(auto==0){
		  		
		  		prefEditor.putString("user_name", null);  
			    prefEditor.putString("password", null);
			    prefEditor.putString("user_id", null);
			  	prefEditor.putInt("number", 3);
		  		
		  	/*}else{
		  		
			  	prefEditor.putInt("number", 0);
		  		
		  	}*/
		  	 
		  	prefEditor.putInt("bump", 0); 
		  	
		  	prefEditor.commit();  
		  
		 finish();
		  
	  }});
	
	builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
		
		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			  
			  dialog.dismiss();
			  
			  
				   return;
				
				
				
		        
			  
		  }});
	
	
	
	builder.setMessage("You want to exit application").create().show();
  }
  
  public void alertFunction(String message,int check){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		if(check ==0){
			builder.setTitle("Warning");
		}
		
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
  
  
}
