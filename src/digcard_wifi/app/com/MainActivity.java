package digcard_wifi.app.com;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bump.api.BumpAPIIntents;
import com.bump.api.IBumpAPI;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.LocalActivityManager;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.tapstream.sdk.*;




public class MainActivity extends TabActivity implements Runnable{
	
	//variable declaration
	
	int position;
	int orientation;
	int total=0;
	String imageResult,user_id;
	private boolean connectivity;
	private int appDestroy=0;
	String recievedData=null;
	TextView titleText;
	Object data=null;
	Object reciverData=null;
	private IBumpAPI api;
	TabHost tabhost;
	private static ProgressDialog progress;
	static ContentResolver resolver;
	static LocalActivityManager manager;
	static Application application;
	private static final int DOWNLOAD=0;
    Context context;
    InputStream imageIs;
    CopyOnWriteArrayList<HashMap<String, String>> transferList = new CopyOnWriteArrayList<HashMap<String, String>>();
    IntentFilter filter;
    private static MainActivity INSTANCE;
    
    private TimeoutHandler timehandler;
    
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		//to remove title bar
		boolean tablet=isTablet(this);
		if(!tablet){
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			
		}
		
        //requestFeature(FEATURE_ACTIO_BAR);
		setContentView(R.layout.main_view);
		
		
		Config config = new Config();
		Tapstream.create(getApplication(), "digcardapp", "DMkCSccES1WlPvKgKznZ_g", config);
		
		/*if(tablet){
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}else{
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			
		}*/
		

		//ActionBar actionBar = getActionBar();
		
	    tabhost = getTabHost();
		context=this;
		resolver=context.getContentResolver();
		application=getApplication();
	    manager =getLocalActivityManager();
	    titleText =(TextView) findViewById(R.id.title);
	    
	    //setting bump value to default
	    SharedPreferences bumpSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
	    SharedPreferences.Editor prefUpdateEditor = bumpSettings.edit();
        prefUpdateEditor.putInt("bump", 0); 
        prefUpdateEditor.commit();
        
        INSTANCE = this;
		  //checking tab position 
		  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        String userName = loginSettings.getString("user_name", null);
	        String passWord = loginSettings.getString("password", null);
	        String firstTime = loginSettings.getString("firstTime", null);
	        position=loginSettings.getInt("number", 3);
	        user_id=loginSettings.getString("user_id", null);
	        int check=loginSettings.getInt("check", 0);
	        int fetch=loginSettings.getInt("fetch", 0);
	        int out=loginSettings.getInt("out", 0);
	        String auto_user_id= loginSettings.getString("auto_user_id", null);
	        
	        timehandler = new TimeoutHandler();
	        
	        //checking orientation
	        switch (this.getResources().getConfiguration().orientation) {
	        
			        case Configuration.ORIENTATION_PORTRAIT:
			        
			        	
			        	orientation=1;
			        	break;
			        	
			        case Configuration.ORIENTATION_LANDSCAPE:
			        	
			        	orientation=2;
			        	        	
			        	break;
			        	
			        default:
			        	  try {
							throw new Exception("Unexpected orientation enumeration returned");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		    	   	
    	
	        }
	        
	        //tab creation
	        customiseIndicator("My Contacts", R.drawable.card_choice, NewCardList.class); // tab 0
        
			
			customiseIndicator("Search", R.drawable.fetch_choice, NewFetch.class); // tab 1
			
			customiseIndicator("Registration", R.drawable.register_choice, NewRegister.class); // tab 2
       	 
       	 
     	    customiseIndicator("Login", R.drawable.login_choice, Login.class); // tab 3
     	    
     	    customiseIndicator("My Profile", R.drawable.profile_choice, NewProfile.class); // tab 4
     	   
    	    customiseIndicator("Logout", R.drawable.logout_choice, Logout.class); // tab 5
    	    
    	    //tabhost.getTabWidget().getChildAt(4).setVisibility(View.GONE);
        	//tabhost.getTabWidget().getChildAt(5).setVisibility(View.GONE);
    	    
        	//default tab
        	
    	   /* if (Integer.parseInt(Build.VERSION.SDK) >= Build.VERSION_CODES.HONEYCOMB) {
    	        tabhost.getTabWidget().setShowDividers(LinearLayout.SHOW_DIVIDER_NONE);
    	    }*/

    	    if(Integer.parseInt(Build.VERSION.SDK) >=14){
    	    	
    	    	getTabHost().getTabWidget().setDividerDrawable(R.drawable.empty_divider); 
    	    	
    	    }
    	    
        		
        		if(user_id == null){
            	    if(auto_user_id==null){
            	    	
            	    	tabhost.getTabWidget().getChildAt(4).setVisibility(View.GONE);
        		    	tabhost.getTabWidget().getChildAt(5).setVisibility(View.GONE);
            	    	
            	    }else{
            	    	
            	    	SharedPreferences autoSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
            		    SharedPreferences.Editor prefAutoEditor = autoSettings.edit();
            		    prefAutoEditor.putString("user_id", auto_user_id); 
            		    prefAutoEditor.commit();
            	    	
            	    	tabhost.getTabWidget().getChildAt(2).setVisibility(View.GONE);
        		    	tabhost.getTabWidget().getChildAt(3).setVisibility(View.GONE);
            	    }
    			    
    		    	
        	   }else{
        	   
        	   		tabhost.getTabWidget().getChildAt(2).setVisibility(View.GONE);
    		    	tabhost.getTabWidget().getChildAt(3).setVisibility(View.GONE);
    		    	
        	   } 
        		
        		if(auto_user_id==null&&user_id==null){
        			
        			if(firstTime==null){
                    	
                		tabhost.setCurrentTab(2);
                		
                	}else{
                		
                		tabhost.setCurrentTab(3);
            	
                	}	
        			
        		}else{
        			
        			tabhost.setCurrentTab(4);
        			
        		}
        		
        		
		
	}	
	
	public static MainActivity getInstance(){

		return INSTANCE;

	}
	
	public boolean isTablet(Context context) {  
        return (context.getResources().getConfiguration().screenLayout   
                & Configuration.SCREENLAYOUT_SIZE_MASK)    
                >= Configuration.SCREENLAYOUT_SIZE_LARGE; 
	}

	
	//Tab customization function
	private void customiseIndicator(String string, int tabSearch, Class<?> c) {	
		Resources res = getResources(); // Resource object to get Drawables
	    TabHost tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;
	    Intent intent = new Intent(this, c);

	    // Initialize a TabSpec for each tab and add it to the TabHost
	    spec = tabHost.newTabSpec("tab" + string).setIndicator(string, res.getDrawable(tabSearch)).setContent(intent);
		
		// set the tab indicator
	    View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
		TextView title = (TextView) tabIndicator.findViewById(R.id.title);
		title.setText(string);
		title.setTextSize(12);
		title.setTextColor(Color.WHITE);
		ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon_image);
		icon.setImageResource(tabSearch);
	    spec.setIndicator(tabIndicator);
	    tabHost.addTab(spec);
	    
	}
	
	//To switch between tabs
	public void switchTab(int tab){
		
		tabhost.setCurrentTab(tab);
		
	}
	
	//orientation change handling
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_list, menu);
		
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu (Menu menu) {
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		//general
		int number=loginSettings.getInt("number", 3);
		
		//fetch card
        int down = loginSettings.getInt("check", 0);
        
        //cardList
        int delete = loginSettings.getInt("delete", 0);
        int trans= loginSettings.getInt("transfer", 0);
        
        
        
        
		
        
        menu.getItem(0).setVisible(false);
    	menu.getItem(1).setVisible(false);
    	menu.getItem(2).setVisible(false);
    	menu.getItem(3).setVisible(false);
    	menu.getItem(4).setVisible(false);
    	menu.getItem(5).setVisible(false);
    	menu.getItem(6).setVisible(false);
	    menu.getItem(7).setVisible(false);
	    menu.getItem(8).setVisible(false);
	    menu.getItem(9).setVisible(false);
	    menu.getItem(10).setVisible(false);
	    menu.getItem(11).setVisible(false);
		menu.getItem(12).setVisible(false);
		menu.getItem(13).setVisible(false);
		menu.getItem(14).setVisible(false);
		menu.getItem(15).setVisible(false);
		menu.getItem(16).setVisible(false);
        
        
        if(number==0){
        	
        	
        	
        	String contactData=((NewCardList)getLocalActivityManager().getCurrentActivity()).searchMyContactText();
        	
        	if (delete == 1){
        		
    	    	
    	    	menu.getItem(4).setVisible(true);
    	    	menu.getItem(5).setVisible(true);
    	    	    	    	
    	    }else if(trans ==1){
    	    	
    	    	menu.getItem(4).setVisible(true);
    	    	
    	    	
    	    }else{
    	    	menu.getItem(0).setVisible(true);
    	    	menu.getItem(1).setVisible(true);
    	    	
    	    	if(contactData.length()==0){
    	    		
    	    		menu.getItem(2).setVisible(true);
    	    		
    	    	}
    	    	
    	    	
    	    }
        }else if(number==1){
        		
        	
        	
        	String data=((NewFetch)getLocalActivityManager().getCurrentActivity()).searchText();
        	
        	if (down == 1){
        		
    	     
    	        menu.getItem(10).setVisible(true);
    	        menu.getItem(11).setVisible(true);
    	        
    	    }else {
    	    	    	    	
    	    	menu.getItem(6).setVisible(true);
    	    	
    	    	if(data.length()==0){
    	    		
    	    		menu.getItem(7).setVisible(true);
        		    menu.getItem(8).setVisible(true);
        		    menu.getItem(9).setVisible(true);
    	    		
    	    	}
    		    
    		    
    	    	
    	    }
        	
        }else if(number==2){
        	
        	
        		
	        	
        		menu.getItem(12).setVisible(true);
        		
        	
        }else if(number==3){
        	
        	
        		
        	
    		menu.getItem(16).setVisible(true);
        	
        	//actionBar.hide();
        	
        }else if(number==4){
        	
        	
        	
	        	
	        	menu.getItem(13).setVisible(true);
	        	menu.getItem(14).setVisible(true);
	        	menu.getItem(15).setVisible(false);
        }else if(number==5){
        	
        	
	        	
        	menu.getItem(16).setVisible(true);
        	
        	//actionBar.hide();
        	
        }
        
        
        
        
	    
		    
	    return true;
	}
	
	
	public boolean onMenuItemSelected(int featureId, MenuItem item){
			
			switch(item.getItemId()){
				
				case R.id.menu_download:
					
					
					
					
					((NewFetch)getLocalActivityManager().getCurrentActivity()).download();
					break;
					
				case R.id.menu_download_all:
					
					
					
					((NewFetch)getLocalActivityManager().getCurrentActivity()).showDownloadAlert();
					break;
				case R.id.menu_sync:
					
					
					((NewFetch)getLocalActivityManager().getCurrentActivity()).showSyncAlert();
					break;	
					
				case R.id.refresh:
					
					
					((NewFetch)getLocalActivityManager().getCurrentActivity()).refreshList(1);
					break;
				case R.id.menu_download_button:
					
					
					((NewFetch)getLocalActivityManager().getCurrentActivity()).download_alert();
					break;
					
				case R.id.menu_download_cancel:
		
					
					SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor prefEditor = loginSettings.edit();  
					prefEditor.putInt("check", 0); 
					prefEditor.commit();  
					
					//bottom.setVisibility(View.GONE);
					
					//mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
					 
					// setListAdapter(mAdapter);
					((NewFetch)getLocalActivityManager().getCurrentActivity()).cancelFunction();
					
					 break;
					 
				case R.id.menu_delete:
					
					
					
					((NewCardList)getLocalActivityManager().getCurrentActivity()).delete();
					break;
					
				case R.id.menu_transfer:
					
					
					((NewCardList)getLocalActivityManager().getCurrentActivity()).transfer();
					break;
					
				case R.id.menu_delete_all:
					
					
					((NewCardList)getLocalActivityManager().getCurrentActivity()).deleteAllAlert();
					break;	
					
				/*case R.id.menu_bump:
					
					Toast.makeText(NewCardList.this, "Bump to transfer the Card", Toast.LENGTH_LONG).show();
					insertRecievedData("name");
					break;*/	
					
				case R.id.menu_bump_cancel:
					
					
					((NewCardList)getLocalActivityManager().getCurrentActivity()).bumpCancelAlert();
					break;
					
				case R.id.menu_delete_button:
					
					
					((NewCardList)getLocalActivityManager().getCurrentActivity()).delete_alert();
					break;		
					
					
				case R.id.menu_save:
					
					
					((NewRegister)getLocalActivityManager().getCurrentActivity()).validation();
					break;	
					
					
				case R.id.menu_update:
					
					
					
					((NewProfile)getLocalActivityManager().getCurrentActivity()).validation();	
				     break;	
				     
				case R.id.menu_update_cancel:
					
					
					((NewProfile)getLocalActivityManager().getCurrentActivity()).previousState();
					
					break;	   
					
				/*case R.id.menu_delete_profile:
					
					((NewProfile)getLocalActivityManager().getCurrentActivity()).deleteOption();
					
					break;*/
					
				case R.id.menu_exit:
					
					
					exitApplication();
					break;	
			}
			return super.onMenuItemSelected(featureId, item);
			
			
		}
	
	
	public void exitApplication(){
		
		this.finish();
		
	}
	
	
	//to auto login after coming from background
	
	public void autoLoginTabChange(){
		SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		String auto_user_id= deleteSettings.getString("auto_user_id", null);
		 
		
		if(auto_user_id==null){
			
			
			
        	tabhost.getTabWidget().getChildAt(2).setVisibility(View.VISIBLE);
        	tabhost.getTabWidget().getChildAt(3).setVisibility(View.VISIBLE);
        	tabhost.getTabWidget().getChildAt(4).setVisibility(View.GONE);
        	tabhost.getTabWidget().getChildAt(5).setVisibility(View.GONE);
        	
        	tabhost.setCurrentTab(3);
        	
        }else{
        	
        	/*SharedPreferences autoSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		    SharedPreferences.Editor prefAutoEditor = autoSettings.edit();
		    prefAutoEditor.putString("user_id", auto_user_id); 
		    prefAutoEditor.putInt("number", 4); 
		    prefAutoEditor.commit();*/
		    
		    
		    
		    
			
			
		    
        	tabhost.getTabWidget().getChildAt(2).setVisibility(View.GONE);
        	tabhost.getTabWidget().getChildAt(3).setVisibility(View.GONE);
        	tabhost.getTabWidget().getChildAt(4).setVisibility(View.VISIBLE);
        	tabhost.getTabWidget().getChildAt(5).setVisibility(View.VISIBLE);
        	        	
        	tabhost.setCurrentTab(4);
        	
        }
	}
	
	
	//tab manipulator during logged in and out
	public void changeTab(){
		
		//SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        //int bumpActive= deleteSettings.getInt("bump", 0);
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        String userName = loginSettings.getString("user_name", null);
        String passWord = loginSettings.getString("password", null);
        String auto_user_id= loginSettings.getString("auto_user_id", null);
        String register = loginSettings.getString("register", null);
        int login=loginSettings.getInt("login", 0);
        user_id =loginSettings.getString("user_id", null);
        
        
        
        if(user_id == null){
        	
        	
        	tabhost.getTabWidget().getChildAt(2).setVisibility(View.VISIBLE);
        	tabhost.getTabWidget().getChildAt(3).setVisibility(View.VISIBLE);
        	tabhost.getTabWidget().getChildAt(4).setVisibility(View.GONE);
        	tabhost.getTabWidget().getChildAt(5).setVisibility(View.GONE);
        	
        	tabhost.setCurrentTab(3);
        	
        }else{
        	
        	
        	
        	tabhost.getTabWidget().getChildAt(2).setVisibility(View.GONE);
        	tabhost.getTabWidget().getChildAt(3).setVisibility(View.GONE);
        	tabhost.getTabWidget().getChildAt(4).setVisibility(View.VISIBLE);
        	tabhost.getTabWidget().getChildAt(5).setVisibility(View.VISIBLE);
        	/*if(register!=null){
        		tabhost.setCurrentTab(4);
        	}else if(login==1){
        		
        		tabhost.setCurrentTab(0);
        		
        	}else{
        		
        		tabhost.setCurrentTab(0);
        	}*/
        	
        	tabhost.setCurrentTab(4);
        	
        }
		
        
        
		
	}
	
	@Override
    public void onDestroy() {
       
		SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        
        SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
        SharedPreferences.Editor prefUpdateEditor = loginSettings.edit();
        prefUpdateEditor.putInt("login", 0); 
        prefUpdateEditor.putString("register", null); 
        prefUpdateEditor.commit();
       
        MyAppData appState = ((MyAppData)getApplication());
		appState.setServiceConnect(connection);
		appState.setReciever(receiver);
		int destroy=1;
		 
		bumpServiceCancel(bumpActive,destroy);
		
		File dir = new File(Environment.getExternalStorageDirectory()+"/DigCrop");
		
		if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i = 0; i < children.length; i++) {
	            new File(dir, children[i]).delete();
	        }
	    }
           
       super.onDestroy(); 
    }
	
	
	public void fetchCard(){
			
			SharedPreferences updateSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefUpdateEditor = updateSettings.edit();  
			prefUpdateEditor.putInt("update", 0); 
			prefUpdateEditor.commit();
			
			SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        String user= deleteSettings.getString("user_id", null);
			
	        if(user!=null){
	        	
	        	((NewFetch)getLocalActivityManager().getCurrentActivity()).refreshList(0);
	        	
	        }else{
	        	
	        	((NewFetch)getLocalActivityManager().getCurrentActivity()).showLoading(MainActivity.this, "Not Loged In");
	        	
	        }
			
			
	    	
	    	 
		}



	public void clearProfileData(){
		
		((NewProfile)getLocalActivityManager().getCurrentActivity()).clearData();
	}


	public void fetchCardServer(){
		
		((NewFetch)getLocalActivityManager().getCurrentActivity()).checkServerConnection();
		
	}
	
	public void logOut(){
		
		((Logout)getLocalActivityManager().getCurrentActivity()).show_alert();
		
	}
	
	public Object onRetainNonConfigurationInstance() {
		
		appDestroy=1;
		
		return appDestroy;
		
	}
	
	
	//function to start bump service
	public void bumpServiceStart(){
		
		SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        int recieverActive=deleteSettings.getInt("reciever", 0);
        
        
        
        System.out.println("bumpActive ::"+bumpActive);
        System.out.println("recieverActive ::"+recieverActive);
        
        if(bumpActive==0){
        	
        	
        	MyAppData appState = ((MyAppData)getApplication());
			data=appState.getServiceConnect();
			reciverData=appState.getReciever();
			
			if(data!=null){
				
				connection=(ServiceConnection)data;
				
			}
			
			if(reciverData!=null){
				
				receiver=(BroadcastReceiver)reciverData;
			}
			
			
        	
        	// getApplicationContext().startService(new Intent(IBumpAPI.class.getName()));
			
			connectivity=haveNetworkConnection();
			if(connectivity){
				
				 getApplicationContext().startService(new Intent(IBumpAPI.class.getName()));
				 getApplicationContext().bindService(new Intent(IBumpAPI.class.getName()), connection, Context.BIND_AUTO_CREATE);
		        	
	        		
	        	   filter = new IntentFilter();
			       filter.addAction(BumpAPIIntents.CHANNEL_CONFIRMED);
			       filter.addAction(BumpAPIIntents.DATA_RECEIVED);
			       filter.addAction(BumpAPIIntents.NOT_MATCHED);
			       filter.addAction(BumpAPIIntents.MATCHED);
			       filter.addAction(BumpAPIIntents.CONNECTED);
			       getApplicationContext().registerReceiver(receiver, filter);
				
			   // Toast.makeText(MainActivity.this, "bump service started", Toast.LENGTH_LONG).show();
				/*SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefEditor = loginSettings.edit();  
				prefEditor.putInt("bump", 1);
				prefEditor.putInt("reciever", 1);
				prefEditor.commit(); 
				System.out.println("bump value changed to 1");*/
			}else{
				
				//Toast.makeText(MainActivity.this, "Network Unavailable", Toast.LENGTH_LONG).show();
				alertFunction("No Internet Connection");
				
			}
        	
        	
			
			
        }
		
	}
	
	//function to handle bump cancel
	public void bumpServiceCancel(int bumpActive ,int finish){
		
		int processId=0;
		if(bumpActive == 1){
        	
        	try{
	        	
        		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefEditor = loginSettings.edit();  
				prefEditor.putInt("bump", 0); 
				prefEditor.putInt("reciever", 0); 
				prefEditor.commit(); 
	       	
				System.out.println("bump value changed to 0");
				
				
        		
        		
        		boolean checkConnection=haveNetworkConnection();
        		
        		if(checkConnection){
        			
        			if(api!=null){
    					
    					api.disableBumping();
    					
    				}
        			
        			getApplicationContext().unbindService(connection);
    				
    				getApplicationContext().unregisterReceiver(receiver);
    				
    				
    				
    				
    					
    					getApplicationContext().stopService(new Intent(IBumpAPI.class.getName()));
    					
    				
    				
    				
    				//Toast.makeText(MainActivity.this, "bump service canceled", Toast.LENGTH_LONG).show();
        			
    				
    				ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
    				List<ActivityManager.RunningAppProcessInfo> pids = am.getRunningAppProcesses();
    				   for(int i = 0; i < pids.size(); i++)
    				   {
    				       ActivityManager.RunningAppProcessInfo info = pids.get(i);
    				       if(info.processName.equalsIgnoreCase("dig.app.com")){
    				    	   processId = info.pid;
    				       } 
    				   }
    				   
    				   if(processId !=0){
    					   try{
    						   android.os.Process.killProcess(processId);
    					   }catch(Exception e){
    						   
    					   }
    					   
    				   }
    				
    				
        		}
				
				
				
	       }catch(Exception e){
	    	   
	    		Log.d("BumpTest DigCard","excepton =="+e);
	       	
	       	Log.d("BumpTest DigCard","unbound service crash");

	       	
	       }
        }
		
	}
	
	//object to monitor the state of bump service
	private ServiceConnection connection = new ServiceConnection() {
      
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.i("BumpTest", "onServiceConnected");
            
            
            
           // Toast.makeText(BumpTest.this, "onServiceConnected", Toast.LENGTH_SHORT).show();
            
            System.out.println("callingBumpServie");
			api = IBumpAPI.Stub.asInterface(binder);
            callingBumpServie();
           /* 
            try {
            	
                api.configure("cdd11f8e0e084f3f8e7bc2c8955939b1", "Bump User");
                
            } catch (RemoteException e) {
                Log.w("BumpTest", e);
            }*/
            
        }

        
        public void onServiceDisconnected(ComponentName className) {
            Log.d("Bump Test", "Service disconnected");
            
            SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = loginSettings.edit();  
			prefEditor.putInt("bump", 0);
			prefEditor.putInt("reciever", 0);
			prefEditor.commit();
           // Toast.makeText(MainActivity.this, "Service disconnected", Toast.LENGTH_SHORT).show();
        }
    };

    
    
    private void callingBumpServie(){
    	
    	new Thread(){
    		public void run(){
    			 try {
    				 
    				 
    				 	System.out.println(" configuring bump api ");
    				 	//System.out.println(" android.os.Build.DEVICE " +android.os.Build.DEVICE);
    				 	//System.out.println(" android.os.Build.MODEL " +android.os.Build.MODEL);
    				 	String name = android.os.Build.DEVICE;
    				 	if(name ==null){
    				 		name = android.os.Build.MODEL;
    				 	}
    	                api.configure("cdd11f8e0e084f3f8e7bc2c8955939b1", name);
    	                
    	            } catch (RemoteException e) {
    	            	
    	                Log.w("BumpTest", e);
    	                
    	            }
    	            Log.d("Bump Test", "Service connected");
    		};

			
    		
    	}.start();
    	
    	
    }
   
    public void onStart(){
    	super.onStart();
    	
    	SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        int autoLogin=deleteSettings.getInt("gotoMyprofile", 0);
        if(bumpActive==1){
        	try{
        		
        		if(api!=null){
        			
        			api.enableBumping();
        			
        		}
        		
        		
        		/*boolean checkConnection=haveNetworkConnection();
        		
        		if(checkConnection){
        			
        			getApplicationContext().bindService(new Intent(IBumpAPI.class.getName()), connection, Context.BIND_AUTO_CREATE);
            		getApplicationContext().registerReceiver(receiver, filter);
            		Toast.makeText(this, "registerReceiver", Toast.LENGTH_LONG).show();
        			
        		}*/
        		
        	}catch(RemoteException e){
        		
        		Log.i("MainActivity", "failed to registerReceiver "+e);
        	}
        	
        	
        }
        
        
        if(autoLogin==1){
        	
        	autoLoginTabChange();
        	
        }
    	
    }
    
    
    public void onStop(){
    	super.onStop();
    	
    	SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        String user_id= deleteSettings.getString("user_id", null);
        int reset=deleteSettings.getInt("dntReset", 0);
        int number=deleteSettings.getInt("number", 3);
        String firstTime=deleteSettings.getString("firstTime", null);
        
        if(bumpActive==1){
        	try{
        		
        		if(api!=null){
        			
        		
        			api.disableBumping();
        			
        		}
        		
        		MyAppData appState = ((MyAppData)getApplication());
        		appState.clearApplicationData();
        		
        		/*boolean checkConnection=haveNetworkConnection();
        		
        		if(checkConnection){
        		
		        		getApplicationContext().unbindService(connection);
		        		getApplicationContext().unregisterReceiver(receiver);
		        		Toast.makeText(this, "unregisterReceiver", Toast.LENGTH_LONG).show();
		        		
        		}*/
        		
        	}catch(RemoteException e){
        		
        		Log.i("MainActivity", "failed to unregisterReceiver "+e);
        	}
        	
        	
        }
        
        
        if(user_id==null && reset!=1&&number!=0&&number!=2&&firstTime!=null){
        	
        	
        	SharedPreferences autoSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        	SharedPreferences.Editor prefAutoEditor = autoSettings.edit();
        	prefAutoEditor.putInt("gotoMyprofile", 1);
        	prefAutoEditor.commit();
        	
        }else{
        	
        	
        	SharedPreferences autoSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        	SharedPreferences.Editor prefAutoEditor = autoSettings.edit();
        	prefAutoEditor.putInt("gotoMyprofile", 0);
        	prefAutoEditor.putInt("dntReset", 0);
        	prefAutoEditor.commit();
        }
    	
    	
    }
    
    
 /*   public void onResume(){
    	super.onResume();
    	
    	SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        if(bumpActive==1){
        	try{
        		
        		getApplicationContext().registerReceiver(receiver, filter);
        		Toast.makeText(this, "registerReceiver", Toast.LENGTH_LONG).show();
        	}catch(Exception e){
        		
        		Log.i("MainActivity", "failed to register the broad cast");
        	}
        	
        	
        }
    	
    }
    
    public void onPause(){
    	super.onPause();
    	
    	SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        if(bumpActive==1){
        	try{
        		
        		getApplicationContext().unregisterReceiver(receiver);
        		Toast.makeText(this, "unregisterReceiver", Toast.LENGTH_LONG).show();
        		
        		
        	}catch(Exception e){
        		
        		Log.i("MainActivity", "failed to unregister the broad cast");
        	}
        	
        	
        }
    	
    }
    */
    
    //to handle the data transfer via bump
    private BroadcastReceiver receiver = new BroadcastReceiver() {
    	
    	
    	@Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            try {
            	
                if (action.equals(BumpAPIIntents.DATA_RECEIVED)) {
                	String user=  api.userIDForChannelID(intent.getLongExtra("channelID", 0));
                	Log.i("Bump Test", "Received data from: " + api.userIDForChannelID(intent.getLongExtra("channelID", 0))); 
                    
                  
    				
                    recievedData=new String(intent.getByteArrayExtra("data"));
                    
                    
                    
                	if(recievedData!=null && recievedData.length() > 0){
                		
                		
                		
                		
                		showAlert(user);
                		
                		
                			
                		}
                    	
                	
                	
                	
                    
                } else if (action.equals(BumpAPIIntents.MATCHED)) {
                	
                	 
                    long channelID = intent.getLongExtra("proposedChannelID", 0); 
                   
                    api.confirm(channelID, true);
                 
                    
                } else if (action.equals(BumpAPIIntents.CHANNEL_CONFIRMED)) {
                	
                	 
                	
                    long channelID = intent.getLongExtra("channelID", 0);
                    
                    String transferData=null;
                    
                    try{
                    	
                    	//transferData=sendData();
                    	
                    	 transferData= ((NewCardList)getLocalActivityManager().getCurrentActivity()).sendData();
                    	
                    }catch(Exception e){
                    	
                    	
                    }
                    
                    
                    if(transferData ==null){
                    	
                    	
                    	
                    	
                    	
                    }else{
                    	
                    	api.send(channelID, transferData.getBytes());
                    }
                    
                    
                    
                } else if (action.equals(BumpAPIIntents.NOT_MATCHED)) {
                	
                	
                    
                } else if (action.equals(BumpAPIIntents.CONNECTED)) {
                	
                	
                    api.enableBumping();
                    
                    SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
        			SharedPreferences.Editor prefEditor = loginSettings.edit();  
        			prefEditor.putInt("bump", 1);
        			prefEditor.putInt("reciever", 1);
        			prefEditor.commit();
        			System.out.println("bump value changed to 1");
        			
        			
        			SharedPreferences bumpSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
        			int number=bumpSettings.getInt("number", 3);
        			
        			if(number==0){
        				
        				//((NewCardList)getLocalActivityManager().getCurrentActivity()).fillData();
        				
        			}
        			
                    
                }
                
                
            } catch (RemoteException e) {}
        } 
    };
	
	//to enable and disable bumping
    
    
    public void manipulateBump(int number){
    	
    	SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        //int number= deleteSettings.getInt("number", 3);
        
        try{
        	
        	if(bumpActive==1){
            	
            	if(number==0){
            		if(api!=null){
            			api.enableBumping();
                		//Toast.makeText(this, "bump enabled", Toast.LENGTH_LONG).show();
            			
            		}
            		
            	}else{
            		if(api!=null){
	            		api.disableBumping();
	            		//Toast.makeText(this, "bump disabled", Toast.LENGTH_LONG).show();
            		}
            	}
            }
        	
        }catch(RemoteException e){
        	
        	
        }
        
    	
    	
    }
    
    //to show Alert
    
    public void showAlert(String name){
    	
    	
    	//if(!isFinishing()){
			
			
			
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				 
				
				  dialog.dismiss();
				  
				  
                  try{
                	  
                	  insertRecievedData(recievedData);
                	  
                  }catch(Exception e){
                	  alertFunction("No Internet Connection");
                	  //Toast.makeText(MainActivity.this, "Check your internet connection and bump it again", Toast.LENGTH_LONG).show();
                  }
                  
				  
				  
				  
			  }});
			
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					  
					
					  dialog.dismiss();
				        
					  
				  }});
			
			builder.setTitle("Are You Sure?");
			builder.setMessage("Are You Sure You want to receive data from "+name).create().show();
    		
    	}
    	
    	
   // }
	
    //inserting data received via bump 
    public void insertRecievedData(String data){
    	
    	progress = new ProgressDialog(MainActivity.this);
    	progress.setTitle("Download");
    	progress.setMessage("Please wait..");
    	progress.setCancelable(false);
    	progress.show();
    	
    	//transferList = new CopyOnWriteArrayList<HashMap<String, String>>();
    	
    	//data insertion to sqlite db in another thread
    	Thread thread=new Thread(this);
    	thread.start();
    	
    	//------   to get image data from sql db via Async Task
    	
    	timehandler.sendMessageDelayed(Message.obtain(timehandler, 1),60000);
    	
    	try {
    		JSONArray jArray = new JSONArray(data);
    		JSONObject json_data;
    		String row_id=null;
    		
    		ArrayList<String> strings = new ArrayList<String>();
    		/*for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
    		   String mTitleRaw = cursor.getString(cursor.getColumnIndex(SBooksDbAdapter.KEY_TITLE_RAW));
    		   strings.add(mTitleRaw);
    		}
    		Sting[] mString = (String[]) strings.toArray(new String[strings.size()]);*/

    		
    		for(int i=0; i<jArray.length(); i++){
    			
    			json_data= jArray.getJSONObject(i);
    			
    			
    			row_id=json_data.getString("user_id");
    			
    			if(row_id!=null){
    				
    				strings.add(row_id);
    			
    				if(i==jArray.length()-1){
    					
    					MyAppData appState = ((MyAppData)getApplication());
    					 appState.setCount(row_id);
    					 
    					 //appState.setCurrentItem(item);
    				}
    				
    				//serverConnection(row_id, jArray.length(), i);
    			}
    			
    			
    		}
    		
    		
    		
    		String [] idList=(String[]) strings.toArray(new String[strings.size()]);
    		
    		
    		serverConnection(idList);
    		
    	}catch(JSONException ep){
    		
    		Log.e("log_tag", "Error JAson result "+ep.toString());
    		
        	//Toast.makeText(getBaseContext(), "No Data Found", Toast.LENGTH_LONG).show();
        	
        }catch (ParseException e) {
        	// TODO Auto-generated catch block
        	
    		e.printStackTrace();
    		
    	}	
        
    }
    
  //Calling Async Task for image data retrieval
    //private void serverConnection(String str, int count,int item) {
    private void serverConnection(String[] str) {	
    	 
    	
    	//total=count;
    	connectivity=haveNetworkConnection();
    	
    	if(connectivity){
    		
    		ImageTransfer task = new ImageTransfer();
    		task.execute(str);
    		
    	}else{
    		alertFunction("No Internet Connection");
    		//Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT).show();
    	}
    }

    // checking for Internet connection
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    
    
  //Async Task for image data retrieval
    private class ImageTransfer extends AsyncTask<String, Integer, CopyOnWriteArrayList<HashMap<String, String>>>{

    	
    	
    	@Override
    	protected CopyOnWriteArrayList<HashMap<String, String>> doInBackground(String... strings) {
    		// TODO Auto-generated method stub
    		
    		transferList = new CopyOnWriteArrayList<HashMap<String, String>>();
    		
    		for (String string : strings) {
    			
    			try {
    				
    				
    				DefaultHttpClient httpclient = new DefaultHttpClient();
    				//HttpPost httppost = new HttpPost("http://dev-fsit.com/iphone/myprofile.php");
    				//HttpPost httppost = new HttpPost("http://dev-fsit.com/iphone/digcard/myprofile.php");
    				HttpPost httppost = new HttpPost("http://digcardapp.com/app/myprofile.php");
    				 
    				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
    				nameValuePairs.add(new BasicNameValuePair("user_id", string));
    	            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
    	            
    	            HttpResponse execute = httpclient.execute(httppost);
    	            HttpEntity entity     = execute.getEntity();
    	            imageIs = entity.getContent();
    				
    				
    			}catch(Exception e){
    				
    				e.printStackTrace();
    			}
    			
    			
    			try{
    				
    				BufferedReader reader = new BufferedReader(new InputStreamReader( imageIs, "iso-8859-1"),8); 
    				StringBuilder sb = new StringBuilder();
    	        	sb.append(reader.readLine() + "\n");
    	        	String line="0";
    	        	
    	        	while((line =reader.readLine()) !=null){
    	        		
    	        		sb.append(line + "\n");
    	        		
    	        	}
    				
    	        	imageIs.close();
    	        	
    	        	
    	        	imageResult=sb.toString();
    				
    				
    			}catch(Exception e){
    				
    				Log.e("log_tag", "Error converting result "+e.toString());
    			}
    			
    			
    			try{
    				
    				JSONArray jArray = new JSONArray(imageResult);
    				JSONObject json_data;
    				
    				for(int i=0; i<jArray.length(); i++){
    				
    				HashMap<String, String> map = new HashMap<String, String>();
    				
    				json_data= jArray.getJSONObject(0);
    				
    				int row_id=json_data.getInt("user_id");
    				
    				String card_id=Integer.toString(row_id);
    				
    				String name=json_data.getString("name");
    	        	
    				String designation=json_data.getString("designation");
    	        	
    				String company=json_data.getString("company_name");
    	        	
    				String company_adderss=json_data.getString("company_address");
    	        	
    				String phone=json_data.getString("phone");
    	        	
    				String email=json_data.getString("email");
    	        	
    				String myProfile_pic=json_data.getString("profile_picture");
    	        	
    				String myProfile_logo=json_data.getString("company_logo");
    	        	
    				String myProfile_video=json_data.getString("video");
    	        	
    				String myProfile_description=json_data.getString("description");
    				
    				String myProfile_upadte_date=json_data.getString("date_of_update");
    	        			        	
    	        	
    	        	map.put("id", card_id);
    	        	map.put("name", name);
    	        	map.put("designation", designation);
    	        	map.put("company", company);
    	        	map.put("company_address", company_adderss);
    	        	map.put("phone", phone);
    	        	map.put("email", email);
    	        	map.put("pic", myProfile_pic);
    	        	map.put("logo", myProfile_logo);
    	        	map.put("video", myProfile_video);
    	        	map.put("description", myProfile_description);
    	        	map.put("date", myProfile_upadte_date);
    	        	
    	        	transferList.add(map);
    				}
    			}catch(JSONException ep){
    				
    				Log.e("log_tag", "Error JAson result "+ep.toString());
    				
    	        	//Toast.makeText(getBaseContext(), "No Data Found", Toast.LENGTH_LONG).show();
    	        	
    	        }catch (ParseException e) {
    	        	// TODO Auto-generated catch block
    	        	
    				e.printStackTrace();
    				
    			}
    		}
    		
    		return transferList;
    	}
    	
    	
    	@Override
    	protected void onPostExecute(CopyOnWriteArrayList<HashMap<String, String>> list) {
    		String c_id=null;
    		String c_pic=null;
    		String c_logo=null;
    		
    		
    		//Toast.makeText(MainActivity.this, "array size =="+list.size(), Toast.LENGTH_LONG).show();
    		
    		@SuppressWarnings("rawtypes")
    		Iterator itr = list.iterator();
    		while (itr.hasNext()) {
    		
    			if(list.size() >0){
    				
    				@SuppressWarnings("unchecked")
    				HashMap<String, String> map =(HashMap<String, String>) itr.next();
    				
    				c_id = (String) map.get("id");
    				 c_pic = (String) map.get("pic");
    				 c_logo = (String) map.get("logo");
    				 
    				if(c_id!=null&&c_pic!=null&&c_logo!=null){
    					
    					//----calling Executor thread to download image----\\
    					
    					
    					LogoBitmapManager.TRANSFERLOGOINSTANCE.logoQueueJob(c_logo, c_id);
    					ProfileBitmapManager.TRANSFERIMAGEINSTANCE.profileQueueJob(c_pic, c_id);
    		           
    					
    					
    					
    				}
    				
    			}
    		}
    	}
    		
    	}
    
    
  //------Executor thread to download Logo image--\\

    public enum LogoBitmapManager {  
    	 
    	TRANSFERLOGOINSTANCE;  
          Bitmap logoBitmap; 
          
          private final ExecutorService logoPool;  
          
          LogoBitmapManager() {  
                
        	  logoPool = Executors.newFixedThreadPool(5);  
            } 
            
          
          public void logoQueueJob(final String url, final String c_id){
        	  
        	  
        	  
        	  
        	  final Handler handler = new Handler() {  
                    @Override  
                    public void handleMessage(Message msg) {  
                        
                       
                            if (msg.obj != null) { 
                            	
                            
                            	ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
    	       					 ((Bitmap) msg.obj).compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
    	       					 byte [] byteArray = byteArrayOutput.toByteArray();
    	       					 //String imageString=Base64.encodeBytes(byteArray);  
    	       					 
    	       					ContentValues values=new ContentValues();
    	       					values.put(CardTable.KEY_LOGO, byteArray);
    	       					
    	       					Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + c_id);
    	       					 
    	       					 resolver.update(queryUri, values, null, null);
                            } else {  
                                
                                Log.d(null, "fail " + url);  
                            }  
                        }  
                      
                }; 
                
                logoPool.submit(new Runnable() {  
                   
                    public void run() {  
                        final Bitmap bmp = logoDownloadBitmap(url);  
                        Message message = Message.obtain();  
                        message.obj = bmp;  
                         
          
                        handler.sendMessage(message);  
                    }  
                });
        	  
          }
          
          
          private Bitmap logoDownloadBitmap(String url) {  
                try {  
                	//logoBitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());  
                	
                	
                	//logoBitmap = Bitmap.createScaledBitmap(logoBitmap, 34, 34, true);
                	
                	BitmapFactory.Options optnsSizeOnly = new BitmapFactory.Options();
                	optnsSizeOnly.inJustDecodeBounds = true;
                	BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, optnsSizeOnly);

                	final int REQUIRED_SIZE = 80;
                			 int width_tmp = optnsSizeOnly.outWidth;
                			 int height_tmp = optnsSizeOnly.outHeight;
                			 
                			 int scale = 1;
                			 
                			 while (true) {
                		            if (width_tmp / 2 < REQUIRED_SIZE
                		               || height_tmp / 2 < REQUIRED_SIZE) {
                		                break;
                		            }
                		            width_tmp /= 2;
                		            height_tmp /= 2;
                		            scale *= 2;
                		        }
                			 
                			 BitmapFactory.Options o2 = new BitmapFactory.Options();
                			 o2.inSampleSize = scale;
                			 logoBitmap= BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, o2); 
                	
                			 
                	/*ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
                	logoBitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
    				 byte [] byteArray = byteArrayOutput.toByteArray();
    				 String imageString=Base64.encodeBytes(byteArray);       
                      logoList.add(imageString); 
                    */
                		
                			 
                    return logoBitmap; 
                    
                } catch (MalformedURLException e) {  
                    e.printStackTrace();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                }  
          
                return null;  
            } 
    	 
     }


    //------Executor thread to download profile pic --\\

    public enum ProfileBitmapManager {  
    	 
    	TRANSFERIMAGEINSTANCE;  
          Bitmap logoBitmap; 
          
          private final ExecutorService picturePool;  
          
          ProfileBitmapManager() {  
                
        	  picturePool = Executors.newFixedThreadPool(5);  
            } 
            
          
          public void profileQueueJob(final String url, final String c_id){
        	  
        	  
        	  
        	  
        	  final Handler handler = new Handler() {  
                    @Override  
                    public void handleMessage(Message msg) {  
                        
                       
                            if (msg.obj != null) { 
                            	
                            
                            	ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
    	       					 ((Bitmap) msg.obj).compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
    	       					 byte [] byteArray = byteArrayOutput.toByteArray();
    	       					 //String imageString=Base64.encodeBytes(byteArray);  
    	       					 
    	       					ContentValues values=new ContentValues();
    	       					values.put(CardTable.KEY_PROFILE_PIC, byteArray);
    	       					
    	       					Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + c_id);
    	       					 
    	       					 resolver.update(queryUri, values, null, null);
    	       					 
    	       					String contactData=((NewCardList)manager.getCurrentActivity()).searchMyContactText();
    	       	    			
    	       	    			if(contactData.length()==0){
    	       	    				
    	       	    				((NewCardList)manager.getCurrentActivity()).fillData();
    	       	    				
    	       	    			}else{
    	       	    				
    	       	    				((NewCardList)manager.getCurrentActivity()).doSearch(contactData);
    	       	    				
    	       	    			}
    	       					
    	       						//((NewCardList)manager.getCurrentActivity()).fillData();
    	       						MyAppData appState = ((MyAppData)application);
    	       					    String count=appState.getCount();
    	       					   
    	       					    if(c_id.equals(count)){
    	       					    	
    	       					    	try{
    		       							
    		       							progress.dismiss();
    		       							progress=null;
    		       							
    		       						}catch(Exception e){
    		       							
    		       						}
    	       					    	
    	       					    }
    	       						
    	       					//}
                            } else {  
                                
                                Log.d(null, "fail " + url);  
                                
                                MyAppData appState = ((MyAppData)application);
	       					    String count=appState.getCount();
	       					   
	       					    if(c_id.equals(count)){
	       					    	
	       					    	try{
		       							
		       							progress.dismiss();
		       							progress=null;
		       							
		       						}catch(Exception e){
		       							
		       						}
	       					    	
	       					    }
                            }  
                        }  
                      
                }; 
                
                picturePool.submit(new Runnable() {  
                      
                    public void run() {  
                        final Bitmap bmp = logoDownloadBitmap(url);  
                        Message message = Message.obtain();  
                        message.obj = bmp;  
                        
          
                        handler.sendMessage(message);  
                    }  
                });
        	  
          }
          
          
          private Bitmap logoDownloadBitmap(String url) {  
                try {  
                	//logoBitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());  
                	
                	
                	//logoBitmap = Bitmap.createScaledBitmap(logoBitmap, 34, 34, true);
                	
                	BitmapFactory.Options optnsSizeOnly = new BitmapFactory.Options();
                	optnsSizeOnly.inJustDecodeBounds = true;
                	BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, optnsSizeOnly);

                	final int REQUIRED_SIZE = 80;
                			 int width_tmp = optnsSizeOnly.outWidth;
                			 int height_tmp = optnsSizeOnly.outHeight;
                			 
                			 int scale = 1;
                			 
                			 while (true) {
                		            if (width_tmp / 2 < REQUIRED_SIZE
                		               || height_tmp / 2 < REQUIRED_SIZE) {
                		                break;
                		            }
                		            width_tmp /= 2;
                		            height_tmp /= 2;
                		            scale *= 2;
                		        }
                			 
                			 BitmapFactory.Options o2 = new BitmapFactory.Options();
                			 o2.inSampleSize = scale;
                			 logoBitmap= BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, o2); 
                    return logoBitmap;  
                } catch (MalformedURLException e) {  
                    e.printStackTrace();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                }  
          
                return null;  
            } 
    	 
     }


    //Thread to insert bump data to sqlite db.
 
    public void run() {
    	// TODO Auto-generated method stub
    	
    	
    	String cursor_userId;
    	byte[] decodedPicByte = null;
    	
    	try {
    		JSONArray jArray = new JSONArray(recievedData);
    		JSONObject json_data;
    		
    		for(int i=0; i<jArray.length(); i++){
    			
    			json_data= jArray.getJSONObject(i);
    			
    			
    			String row_id=json_data.getString("user_id");
    			
    			
    			
    			String name=json_data.getString("name");
    			
    			
    			String designation=json_data.getString("designation");
    			
    			
    			String company=json_data.getString("companyname");
    			
    			
    			String company_adderss=json_data.getString("companyaddress");
    			
    			
    			String phone=json_data.getString("phone");
    			
    			
            	String email=json_data.getString("email");
            	
            	
            	String description=json_data.getString("description");
            	
            	
            	String updateDate=json_data.getString("dateofupdate");
            	
            	String comment=json_data.getString("comment");
            	
            	String webUrl=json_data.getString("weburl");
            	
            	String office=json_data.getString("office");
            	String fax=json_data.getString("fax");
            	String video=json_data.getString("video");
            	
            	String[] information= new String[] {CardTable.KEY_NAME, CardTable.KEY_USERID};
            	
            	Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + row_id);
    	        Cursor c = getContentResolver().query(queryUri, information, null, null, null);
            	
            	
    	        String image="/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAAiACIDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7EubmW7neaZzJK5yzN1/z/n6Tabpd3q1yILOBp5epC9FHuegFVa9M+E8IXTL6Tby0wXOOwUf416lSfs4XR50VzOxx+teDNQ0GyS6uVQox2sIsts/3jjisMce2P0r33Uolm0+6R1Dq0TAqRnPBrwFPuj6Cs6FV1E7lTio7GzH4v1aKNUF42FAAzgmisccUVryx7CTYV6N8KNTXyLzT2IDK3nIPUHg/rj864TTtIvNVmMVnbSTuOu1eF+p6CvTfAvg6Xw8J7i8MbXUoCqIySEXuM98nH5VjXlHkae46ad7m9r2pro+j3d4xA8tDtz3Y8AfnivCeSST17mvdtd0ldb0m5smO3zVwrH+FhyD+eK8j1jwbquiBmmt/NhH/AC2g+ZQPfjI/GssNKKTTepdRNmKOKKADiiu0xPfbC3it7fbFGkSlixCKACcnmrFFFeLLdnYtgoooqRoxjawgkCGMAf7I96KKK0IR/9k=";
    	       
    	        try {
    	        	
    					decodedPicByte = Base64.decode(image.getBytes());
    				
    			
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    	      
    	       
    	        ContentValues values=new ContentValues();
    	 		values.put(CardTable.KEY_USERID, row_id);
    	 		values.put(CardTable.KEY_NAME, name);
    	 		values.put(CardTable.KEY_EMAIL, email);
    	 		values.put(CardTable.KEY_NUMBER, phone);
    	 		values.put(CardTable.KEY_PROFILE_PIC, decodedPicByte);
    	 		values.put(CardTable.KEY_DESIGNATION, designation);
    	 		values.put(CardTable.KEY_VIDEO, video);
    	 		values.put(CardTable.KEY_COMPANY, company);
    	 		values.put(CardTable.KEY_ADDRESS, company_adderss);
    	 		values.put(CardTable.KEY_LOGO, decodedPicByte);
    	 		values.put(CardTable.KEY_DESCRIPTION, description);
    	 		values.put(CardTable.KEY_UPDATE_DATE, updateDate);
    	 		values.put(CardTable.KEY_COMMENTS, comment);
    	 		values.put(CardTable.KEY_WEB_ADDRESS, webUrl);
    	 		values.put(CardTable.KEY_OFFICE, office);
    	 		values.put(CardTable.KEY_FAX, fax);
    	 		
    	 		if ( c != null && c.getCount() > 0 ) {
    	 			
    	 			
    	 			if (c.moveToFirst()) {
    	 				do {
    	 					
    	 					cursor_userId=c.getString(c.getColumnIndexOrThrow(CardTable.KEY_USERID));
    	 					
    	 					if(row_id.equals(cursor_userId)){
    	 		        		resolver.update(queryUri, values, null, null);
    	 		            }else {
    	 		        		resolver.insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
    	 		           	}
    	 					
    	 			    }while(c.moveToNext());
    	 				
    	 			}
    	 			
    	 			
    	 			
    	 		}else {
    	 			
    	 			
    	 					 			
    	 			resolver.insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
    	 		}
    	 		
    	 		c.close();
    	 		
    	 		
    	 		
    	 		
    		}
    		
    		
    		
    	} catch(JSONException ep){
    		
    		Log.e("log_tag", "Run Error JAson result "+ep.toString());
    		
        	//Toast.makeText(getBaseContext(), "No Data Found", Toast.LENGTH_LONG).show();
        	
        }catch (ParseException e) {
        	// TODO Auto-generated catch block
        	
    		e.printStackTrace();
    		
    	}
        
       // if(!isFinishing()){
    		
        	handler.sendEmptyMessage(DOWNLOAD);
        	
    		//((NewCardList)getLocalActivityManager().getCurrentActivity()).fillData();
    	//}
    	
    }


    //-----message handler for thread----\\
    
    public  class  TimeoutHandler extends Handler{
    	 public void handleMessage(Message msg) {
    		 switch(msg.what){
    		 	case 1:
    		 			try{
    		 				
							progress.dismiss();
							progress=null;
							alertFunction("Server Connection Failed.Please check your internet connection and try again");
						}catch(Exception e){
							
						}
    		 		
    		 		break;
    		 	default:
    		 		try{
						
						progress.dismiss();
						progress=null;
						
					}catch(Exception e){
						
					}
    		 }
    	 }
    }

    private Handler handler=new Handler(){
    	
    	public void handleMessage(Message msg){
    		switch(msg.what){
    		
    		case DOWNLOAD:
    			
    			String contactData=((NewCardList)getLocalActivityManager().getCurrentActivity()).searchMyContactText();
    			
    			if(contactData.length()==0){
    				
    				((NewCardList)getLocalActivityManager().getCurrentActivity()).fillData();
    				
    			}else{
    				
    				((NewCardList)getLocalActivityManager().getCurrentActivity()).doSearch(contactData);
    				
    			}
    			
    			
    			
    			break;
    		
    		}
    	}
    };
    
    public void manipulateTitle(String title){
    	titleText.setText(title);
    }
	
    public void alertFunction(String message){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		builder.setTitle("Warning");
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
	
	
}