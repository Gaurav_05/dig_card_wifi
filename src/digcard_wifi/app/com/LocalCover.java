package digcard_wifi.app.com;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;

import com.example.coverflow.CoverFlow;




public class LocalCover extends Activity{
	
	
	CoverFlow localCoverFlow;
	ImageCoverAdapter localCoverImageAdapter;
	Cursor cur;
	String row_id;
	Object data;
	int type;
	
	String coverId,coverName,coverDesignation,coverCompany,coverPhone,coverProfile_pic,coverLogo;
	 ArrayList<HashMap<String, String>> myCoverCardList=new  ArrayList<HashMap<String, String>>();  ;  
	
	@SuppressWarnings("unchecked")
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		 type=getscrOrientation();
		 
		 /*if(type==2){//portrait
			 
			 this.finish();//finishing coverflow
		 }*/
		setContentView(R.layout.local_cover);
		localCoverFlow =  (CoverFlow) findViewById(R.id.local_coverflow);
		
		GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {0xFFf6f8fb,0xFFf6f8fb});
        gd.setCornerRadius(0f);
        System.out.println("Local cover create");
        
        MyAppData myAppState = ((MyAppData)getApplication());
		data=myAppState.getCardlistState();
		//cur=(Cursor)data;
		myCoverCardList=(ArrayList<HashMap<String, String>>)data;
        //localCoverFlow.setBackgroundDrawable(gd);
        
        localCoverFlow.setOnItemClickListener(new OnItemClickListener() {
	           

			
			public void onItemClick(AdapterView<?> l, View v, int position,
					long id) {
				// TODO Auto-generated method stub
				int coverCenter=localCoverFlow.getdetail(v);
				
				if(coverCenter==1){
					
					
					
					
					cur.moveToFirst();
					
						int count=cur.getCount();
						//System.out.println("number  == "+count);
						
					  long value=id % count;             
					 // System.out.println("selected id  == "+id);
					  
					  String[] projection=new String[] {CardTable.KEY_ROWID};
					  
					  Uri uri=CardManagingProvider.CONTENT_URI_DETAIL;
					  Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
					  startManagingCursor(cursor);
					  if(cursor.moveToFirst()){
						  
						  cursor.moveToPosition((int) value);
						   row_id=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_ROWID));
						   //System.out.println("row_id :"+row_id);
						   
						   
					  }	    
					//Toast.makeText(LocalCover.this, "Item selected"+position, Toast.LENGTH_SHORT).show();
					
					Intent edit_intent = new Intent(LocalCover.this, LocalCard.class);
					
					
					Uri listUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL_LOCAL + "/" + row_id);
					edit_intent.putExtra(CardManagingProvider.CONTENT_ITEM_TYPE, listUri);
					
					startActivity(edit_intent);
					
					
					
					
				}else{
					
					//Toast.makeText(LocalCover.this, "Item focused"+position, Toast.LENGTH_SHORT).show();
				}
			}
          });
        
        
        
        
        if(type == 2){
			
        	Intent ntnt= new Intent(LocalCover.this, MainActivity.class);
    		ntnt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(ntnt);
			this.finish();
			return;
		}else if(type ==3){
			
			fillData();			
			
		}
        
        
	}
	
	
	
	
	@Override
	public void onBackPressed() {
	   return;
	}
	
	

	private void fillData() {
		// TODO Auto-generated method stub
		
		//System.out.println("Local cover on fillData");
		String[] projection=new String[] {CardTable.KEY_ROWID, CardTable.KEY_NAME, CardTable.KEY_DESIGNATION, CardTable.KEY_COMPANY, CardTable.KEY_NUMBER,CardTable.KEY_PROFILE_PIC, CardTable.KEY_LOGO};
		
		
		Uri uri=CardManagingProvider.CONTENT_URI_DETAIL;
		
		if(myCoverCardList==null){
			
			System.out.println("Local cover fillData list data null");
			
			 //getContentResolver().insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
	 		cur = getContentResolver().query(uri, projection, null, null, null);
			
	 		if(cur!=null&&cur.getCount()>0){
	 			
	 			//System.out.println("Local cover fillData cursor");
	 			
	 			myCoverCardList=new ArrayList<HashMap<String, String>>();
				
	 		    if (cur.moveToFirst()) {
	 		        // loop until it reach the end of the cursor
	 		        do {
	 		        	
	 		        	coverId=cur.getString(cur.getColumnIndexOrThrow(CardTable.KEY_ROWID));
	 		        	coverName=cur.getString(cur.getColumnIndexOrThrow(CardTable.KEY_NAME));
	 		        	coverDesignation=cur.getString(cur.getColumnIndexOrThrow(CardTable.KEY_DESIGNATION));
	 		        	coverCompany=cur.getString(cur.getColumnIndexOrThrow(CardTable.KEY_COMPANY));
	 		        	coverPhone=cur.getString(cur.getColumnIndexOrThrow(CardTable.KEY_NUMBER));
	 		        	coverProfile_pic=cur.getString(cur.getColumnIndexOrThrow(CardTable.KEY_PROFILE_PIC));
	 		        	coverLogo=cur.getString(cur.getColumnIndexOrThrow(CardTable.KEY_LOGO));
	 		        		
	 		        	HashMap<String, String> map = new HashMap<String, String>();
							
							
	 		        	map.put("id", coverId);
			        	map.put("name", coverName);
			        	map.put("designation", coverDesignation);
			        	map.put("company", coverCompany);
			        	map.put("phone", coverPhone);
			        	map.put("pic", coverProfile_pic);
			        	map.put("logo", coverLogo);
			        	
			        	myCoverCardList.add(map);
	 		        	
	 		        } while (cur.moveToNext());
	 		    }

	 		    // make sure to close the cursor
	 		   //cursor.close();
	 			
	 			//localCoverImageAdapter =  new ImageCoverAdapter(LocalCover.this,cur);
	 		    int size=myCoverCardList.size();
	 	    	
	 		   localCoverImageAdapter =  new ImageCoverAdapter(LocalCover.this,myCoverCardList);
	 	 		localCoverFlow.setAdapter(localCoverImageAdapter);
	 	    	
	 	 		localCoverFlow.setSpacing(-40);
	 	 		localCoverFlow.setSelection(size*100, true);
	 	 		localCoverFlow.setAnimationDuration(1000);
	 			
	 		}
			
		}else{
			System.out.println("Local cover fillData cursor data not null");
			
			//if(cur.getCount()>0){
			if(myCoverCardList.size()>0){
	 			
	 			//System.out.println("Local cover fillData cursor");
	 			
	 			//localCoverImageAdapter =  new ImageCoverAdapter(LocalCover.this,cur);
				localCoverImageAdapter =  new ImageCoverAdapter(LocalCover.this,myCoverCardList);
	 	    	
	 	 		localCoverFlow.setAdapter(localCoverImageAdapter);
	 	    	
	 	 		localCoverFlow.setSpacing(-40);
	 	 		localCoverFlow.setSelection(3, true);
	 	 		localCoverFlow.setAnimationDuration(1000);
	 			
	 		}
		}
		
 		
 		
		
	}	
	
	
	
	public int getscrOrientation()
	{
	Display getOrient = getWindowManager().getDefaultDisplay();

	int orientation = getOrient.getOrientation();

	
	if(getOrient.getWidth()==getOrient.getHeight()){
		
		orientation = 1;
	
	}else{ //if widht is less than height than it is portrait
	if(getOrient.getWidth() < getOrient.getHeight()){
		
		orientation = 2;
	
	}else{ // if it is not any of the above it will defineitly be landscape
		
		orientation = 3;
	
	}
	}
	
	
	return orientation;
	}
	
	
	
	public class ImageCoverAdapter extends BaseAdapter {
		
		ArrayList<HashMap<String, String>> cover = null;
		Bitmap resizedImage,background;
		private Context localContext;
		ImageView imageView;

		
		public ImageCoverAdapter(Context context, ArrayList<HashMap<String, String>> list) {
			// TODO Auto-generated constructor stub
			
			this.localContext=context;
			this.cover=list;
			
		}
		
		
		public int getCount() {
			// TODO Auto-generated method stub
			return Integer.MAX_VALUE;
		}

		
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position % cover.size();
		}

		
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		
private Bitmap cropeFunction(String str){
			
			//System.out.println("ImageAdapter crope method");
			
			
		
			
			try {
				
				byte[] imageAsBytes;
				imageAsBytes = Base64.decode(str.getBytes());
				
			           // BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
				
				Bitmap bitmapOrg = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
				
				int width = bitmapOrg.getWidth();
				
				int height = bitmapOrg.getHeight();
				
				int required=40;
				
				Matrix matrix = new Matrix();

				float scaleWidth = ((float) required) / width;
			    float scaleHeight = ((float) required) / height;

				
				matrix.postScale(scaleWidth, scaleHeight);
				
				//System.out.println("Local cover crope method bitmap create");
				resizedImage = Bitmap.createBitmap(bitmapOrg, 0, 0, 
	                    width, height, matrix, true); 

				//return resizedBitmap;
			    				    
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			return  resizedImage;
		
			
		
		
				
	}
		
		
		
		
		/*private Bitmap createReflectedImages(Bitmap bit){
			
			
			System.out.println("ImageAdapter reflection method");
			
			int new_width=bit.getWidth();
			
			int new_height=bit.getHeight();
			
			
			//This will not scale but will flip on the Y axis
    		
			Matrix matrix = new Matrix();
			
			matrix.preScale(1, -1);
			
			System.out.println("ImageAdapter reflection method create bitmap " );
			
			Bitmap reflectionImage = Bitmap.createBitmap(bit, 0, new_height/2, new_width, new_height/2, matrix, false);
			
			return reflectionImage;
			
			
		}*/
		
		
		private Bitmap adjustBitmap(Bitmap val){
			
			//System.out.println("ImageAdapter adjustBitmap method");
			
			int width=val.getWidth()+20;
			int height=val.getHeight()+20;
			
			Matrix matrix = new Matrix();

			float scaleWidth = ((float) width) /val.getWidth() ;
		    float scaleHeight = ((float) height) / val.getHeight();
		   // System.out.println("scaleWidth :"+scaleWidth);
		   // System.out.println("scaleHeight :"+scaleHeight);
		    
		    matrix.postScale(scaleWidth, scaleHeight);
			
		   // System.out.println("ImageAdapter adjustBitmap method create bitmap");
			
			Bitmap adjust = Bitmap.createBitmap(val, 0, 0, val.getWidth(), val.getHeight(), matrix, false);
			
			return adjust;
			
			
		}
		
		
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			
			HashMap<String, String> map = this.cover.get(position % cover.size());
			
			String l_name = map.get("name").toString();
		    String l_number = map.get("phone").toString();
		    String l_company = map.get("company").toString();
		    String l_designation = map.get("designation").toString();
		    String l_pic = map.get("pic").toString();
		    String l_logo = map.get("logo").toString();
		    
		    
		    Bitmap bit=cropeFunction(l_pic);
		    
		    Bitmap log=cropeFunction(l_logo);
		    
		    background = BitmapFactory.decodeResource(getResources(), R.drawable.cover);
		    
		    Bitmap new_back=adjustBitmap(background);
		    
		  //Bitmap localImageReflection=createReflectedImages(background);
		    
		    Bitmap localBitmapWithReflection = Bitmap.createBitmap(new_back.getWidth(), new_back.getHeight(), Config.ARGB_8888);
		    
		    Canvas canvas = new Canvas(localBitmapWithReflection);
		    
		    canvas.drawBitmap(new_back, 0, 0, null);
			
			Paint name = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG); 
			
			name.setTextAlign(Paint.Align.CENTER );
			name.setStyle(Paint.Style.FILL);
			name.setARGB(255, 0, 0, 255);
			name.setTextSize(10); 
			name.setTypeface(Typeface.SERIF);
			name.setFakeBoldText(true);
			canvas.drawText(l_name, new_back.getWidth()/2, 20, name); 
			
			
			canvas.drawBitmap(bit,new_back.getWidth()/6, new_back.getHeight()/4, null);
			
			Paint desig=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
			
			desig.setColor(Color.BLACK); 
			desig.setTypeface(Typeface.SERIF);
			desig.setFakeBoldText(true);
			desig.setTextSize(7); 
			desig.setStyle(Paint.Style.FILL);
			canvas.drawText(l_designation, new_back.getWidth()/2, new_back.getHeight()/4+7, desig);
			
			Paint comp=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
			
			comp.setColor(Color.BLACK); 
			comp.setTypeface(Typeface.SERIF);
			comp.setFakeBoldText(true);
			comp.setTextSize(7); 
			comp.setStyle(Paint.Style.FILL);
			canvas.drawText(l_company, new_back.getWidth()/2, new_back.getHeight()/4+17, comp);
			
			
			Paint numb=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
			
			numb.setColor(Color.BLACK); 
			numb.setTypeface(Typeface.SERIF);
			numb.setFakeBoldText(true);
			numb.setTextSize(7); 
			numb.setStyle(Paint.Style.FILL);
			canvas.drawText(l_number, new_back.getWidth()/2, new_back.getHeight()/4+27, numb);
			
			
			Paint logo=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
			
			logo.setColor(Color.BLACK); 
			logo.setTypeface(Typeface.SERIF);
			logo.setFakeBoldText(true);
			logo.setTextSize(7); 
			logo.setStyle(Paint.Style.FILL);
			canvas.drawText("Logo", new_back.getWidth()/6+6, (new_back.getHeight()/6)*5-5, logo);
			
			canvas.drawBitmap(log, new_back.getWidth()/2+10, (new_back.getHeight()/3)*2, null);
			
			imageView = new ImageView(localContext);
				
			imageView.setImageBitmap(localBitmapWithReflection);
			
			imageView.setLayoutParams(new CoverFlow.LayoutParams(new_back.getWidth(), new_back.getHeight()));
		
			imageView.setScaleType(ScaleType.MATRIX);
			
			return imageView;
		}
		
		
	}
	
	
	/*
	public class ImageCoverAdapter extends BaseAdapter {
		
		private final Cursor c;
		Bitmap resizedImage,background;
		private final int mNameIndex;
		private final int mNumberIndex;
		private final int mCompanyIndex;
		private final int mDesigIndex;
		private final int mPicIndex;
		private final int mLogoIndex;
		private Context localContext;
		ImageView imageView;

		public ImageCoverAdapter(Context localCover, Cursor cur) {
			// TODO Auto-generated constructor stub
			
			this.c=cur;
			this.localContext=localCover;
			this.mNameIndex = c.getColumnIndexOrThrow(CardTable.KEY_NAME);
		    this.mNumberIndex = c.getColumnIndexOrThrow(CardTable.KEY_NUMBER);
		    this.mDesigIndex = c.getColumnIndexOrThrow(CardTable.KEY_DESIGNATION);
		    this.mPicIndex = c.getColumnIndexOrThrow(CardTable.KEY_PROFILE_PIC);
		    this.mCompanyIndex = c.getColumnIndexOrThrow(CardTable.KEY_COMPANY);
		    this.mLogoIndex = c.getColumnIndexOrThrow(CardTable.KEY_LOGO);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Integer.MAX_VALUE;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position % c.getCount();
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		
		
		private Bitmap cropeFunction(String str){
			
			//System.out.println("ImageAdapter crope method");
			
			
		
			
			try {
				
				byte[] imageAsBytes;
				imageAsBytes = Base64.decode(str.getBytes());
				
			           // BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
				
				Bitmap bitmapOrg = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
				
				int width = bitmapOrg.getWidth();
				
				int height = bitmapOrg.getHeight();
				
				int required=40;
				
				Matrix matrix = new Matrix();

				float scaleWidth = ((float) required) / width;
			    float scaleHeight = ((float) required) / height;

				
				matrix.postScale(scaleWidth, scaleHeight);
				
				//System.out.println("Local cover crope method bitmap create");
				resizedImage = Bitmap.createBitmap(bitmapOrg, 0, 0, 
	                    width, height, matrix, true); 

				//return resizedBitmap;
			    				    
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			return  resizedImage;
		
			
		
		
				
	}
		
		
		
		
		private Bitmap createReflectedImages(Bitmap bit){
			
			
			System.out.println("ImageAdapter reflection method");
			
			int new_width=bit.getWidth();
			
			int new_height=bit.getHeight();
			
			
			//This will not scale but will flip on the Y axis
    		
			Matrix matrix = new Matrix();
			
			matrix.preScale(1, -1);
			
			System.out.println("ImageAdapter reflection method create bitmap " );
			
			Bitmap reflectionImage = Bitmap.createBitmap(bit, 0, new_height/2, new_width, new_height/2, matrix, false);
			
			return reflectionImage;
			
			
		}
		
		
		private Bitmap adjustBitmap(Bitmap val){
			
			//System.out.println("ImageAdapter adjustBitmap method");
			
			int width=val.getWidth()+20;
			int height=val.getHeight()+20;
			
			Matrix matrix = new Matrix();

			float scaleWidth = ((float) width) /val.getWidth() ;
		    float scaleHeight = ((float) height) / val.getHeight();
		   // System.out.println("scaleWidth :"+scaleWidth);
		   // System.out.println("scaleHeight :"+scaleHeight);
		    
		    matrix.postScale(scaleWidth, scaleHeight);
			
		   // System.out.println("ImageAdapter adjustBitmap method create bitmap");
			
			Bitmap adjust = Bitmap.createBitmap(val, 0, 0, val.getWidth(), val.getHeight(), matrix, false);
			
			return adjust;
			
			
		}
		
		
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			if (c.moveToPosition(position % c.getCount())) {
				
				String data_name=c.getString(mNameIndex);
				String data_number=c.getString(mNumberIndex);
				String data_designation=c.getString(mDesigIndex);
				String data_company=c.getString(mCompanyIndex);
				String data_pic=c.getString(mPicIndex);
				String data_logo=c.getString(mLogoIndex);
				
				//System.out.println("local cover getView");
				
				Bitmap bit=cropeFunction(data_pic);
			    
			    Bitmap log=cropeFunction(data_logo);
			    
				//Bitmap bit=data_pic;
				
			    background = BitmapFactory.decodeResource(getResources(),
                        R.drawable.cover);
			    
			    Bitmap new_back=adjustBitmap(background);
			    
			    //Bitmap localImageReflection=createReflectedImages(background);
			    
			    Bitmap localBitmapWithReflection = Bitmap.createBitmap(new_back.getWidth(), new_back.getHeight(), Config.ARGB_8888);
			    
			    Canvas canvas = new Canvas(localBitmapWithReflection);
			    
			  //  System.out.println("Canvas created");
			   				
				canvas.drawBitmap(new_back, 0, 0, null);
				
				Paint name = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG); 
				
				name.setTextAlign(Paint.Align.CENTER );
				name.setStyle(Paint.Style.FILL);
				//name.setColor(Color.BLUE); 
				name.setARGB(255, 0, 0, 255);
				name.setTextSize(10); 
				name.setTypeface(Typeface.SERIF);
				name.setFakeBoldText(true);
				canvas.drawText(data_name, new_back.getWidth()/2, 20, name); 
				
				//System.out.println("Canvas created back drawn");
				
				
				//System.out.println("Canvas created pic drawn");
				canvas.drawBitmap(bit,new_back.getWidth()/6, new_back.getHeight()/4, null);
				
				Paint desig=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
				
				desig.setColor(Color.BLACK); 
				desig.setTypeface(Typeface.SERIF);
				desig.setFakeBoldText(true);
				desig.setTextSize(7); 
				desig.setStyle(Paint.Style.FILL);
				canvas.drawText(data_designation, new_back.getWidth()/2, new_back.getHeight()/4+7, desig);
				
				Paint comp=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
				
				comp.setColor(Color.BLACK); 
				comp.setTypeface(Typeface.SERIF);
				comp.setFakeBoldText(true);
				comp.setTextSize(7); 
				comp.setStyle(Paint.Style.FILL);
				canvas.drawText(data_company, new_back.getWidth()/2, new_back.getHeight()/4+17, comp);
				
				
				Paint numb=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
				
				numb.setColor(Color.BLACK); 
				numb.setTypeface(Typeface.SERIF);
				numb.setFakeBoldText(true);
				numb.setTextSize(7); 
				numb.setStyle(Paint.Style.FILL);
				canvas.drawText(data_number, new_back.getWidth()/2, new_back.getHeight()/4+27, numb);
				
				
				Paint logo=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
				
				logo.setColor(Color.BLACK); 
				logo.setTypeface(Typeface.SERIF);
				logo.setFakeBoldText(true);
				logo.setTextSize(7); 
				logo.setStyle(Paint.Style.FILL);
				canvas.drawText("Logo", new_back.getWidth()/6+6, (new_back.getHeight()/6)*5-5, logo);
				
				//canvas.drawBitmap(log,new_back.getWidth()/6, (new_back.getHeight()/3)*2, null);
				
			//	System.out.println("Canvas created logo drawn");
				
				canvas.drawBitmap(log, new_back.getWidth()/2+10, (new_back.getHeight()/3)*2, null);*/
				
				/*Bitmap video=BitmapFactory.decodeResource(getResources(),
                        R.drawable.video_thumbnail);
				
				
				Bitmap profile_video = Bitmap.createBitmap(video, 0, 0, 
						video.getWidth(), video.getHeight(), null, false); 
				
				canvas.drawBitmap(profile_video, new_back.getWidth()/2+10, (new_back.getHeight()/3)*2, null);*/
				
				/*canvas.drawBitmap(localImageReflection,0, 176, null);
				
				
				
				Paint paint = new Paint();
				
				LinearGradient shader = new LinearGradient(5, 174, 5, 210, 0x70d2dff2, 0x00ffffff, TileMode.CLAMP);
				
				//Set the paint to use this shader (linear gradient)
				
				paint.setShader(shader);
				
				//Set the Transfer mode to be porter duff and destination in
			
				paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
				
				//Draw a rectangle using the paint with our linear gradient
				
				canvas.drawRect(5, 174, 165, 210, paint);*/
				
				
			 /*   imageView = new ImageView(localContext);
				
				imageView.setImageBitmap(localBitmapWithReflection);
				
				imageView.setLayoutParams(new CoverFlow.LayoutParams(new_back.getWidth(), new_back.getHeight()));
			
				imageView.setScaleType(ScaleType.MATRIX);
				
				
				//return imageView;
			}
			return imageView;
			
		}
		
		
	}*/
	
	
}
