package digcard_wifi.app.com;


import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bump.api.IBumpAPI;

@SuppressWarnings("serial")
public class NewCardList extends ListActivity implements Serializable{
	
	static ContentResolver resolver;
    Context context;
    int tabPosition;
    RelativeLayout delete_bar,transfer_bar;
    ListView cardListview;
    EditText cardListSearch;
    Button bottom_delete,button_transfer;
    String lowSearch=null;
    Cursor cursor; 
    public ArrayList<Boolean> tick=new ArrayList<Boolean>();
    //private MyCursorAdapter mcAdapter;
    private MyCustomAdapter mcAdapter;
    ArrayList<HashMap<String, String>> transferList = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> myCardList   = new ArrayList<HashMap<String, String>>();  
	ArrayList<HashMap<String, byte[]>> myImageList   = new ArrayList<HashMap<String, byte[]>>();
	private ListView listView = null;
	boolean connection=false;
	int serviceCount=0;
	boolean serviceState=false;
	IBumpAPI api;
	InputStream imageIs;
    String imageresult;
    
    private RelativeLayout contactLayout;
    
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local_layout);
		
		
		
		context=this;
		resolver=context.getContentResolver();
		//this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
		tabPosition=checkSettings.getInt("number", 3);
		
		contactLayout=(RelativeLayout) findViewById(R.id.contact_layout);
		
		cardListview=(ListView) findViewById(android.R.id.list);
		cardListSearch=(EditText) findViewById(R.id.card_search);
		
		
		
		delete_bar=(RelativeLayout) findViewById(R.id.bottom_delete_bar);
		transfer_bar = (RelativeLayout) findViewById(R.id.bottom_transfer_bar);
		
		fillData();
		
		
		
		
		cardListSearch.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
             String searchData=  cardListSearch.getText().toString();
             
             
             /*if(tabPosition ==0 && orientation==1){
            	 
            	 doSearch(searchData);
             }*/
             
            // if(tabPosition ==0 ){
            	 
            	 
            	 doSearch(searchData);
           //  }
             
            
             
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        }); 
		
		bottom_delete=(Button) findViewById(R.id.button_delete);
		button_transfer=(Button) findViewById(R.id.button_transfer);
		
	
		
		
		button_transfer.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				transfer_alert();
					
				
			}
		});
		
		
	}
	
	
	public String searchMyContactText(){
		
		String searchData=  cardListSearch.getText().toString();
		
		return searchData;
	}
	
	
	public void onStart(){
		super.onStart();
		
		
		
		
		
		//cardListSearch.clearFocus();
		contactLayout.requestFocus();	
		

	}
	
	@Override
	public void onBackPressed() {
		
		
		
		
		SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        final int auto= deleteSettings.getInt("auto", 0);
        
        /*
        MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	parent.bumpServiceCancel(bumpActive);
    	*/
    	
		
		//bumpServiceCancel(bumpActive);
		//super.onBackPressed();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(NewCardList.this);
		builder.setTitle("Exit");
		builder.setCancelable(false);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			  
			  dialog.dismiss();
			  
			  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefEditor = loginSettings.edit();  
				
				//if(auto==0){
					prefEditor.putString("user_name", null);  
				    prefEditor.putString("password", null);
				    prefEditor.putString("user_id", null);
					prefEditor.putInt("number", 3); 
					//prefEditor.putInt("bump", 0); 
					
				//}else{
					
					//prefEditor.putInt("number", 0); 
					
					
				//}
				
				prefEditor.commit();
		        
			 finish();
			  
		  }});
		
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
				  
				  
					   return;
					
					
					
			        
				  
			  }});
		
		
		
		builder.setMessage("You want to exit application").create().show();
	
	
		
		
	}
	
	/*public void bumpServiceCancel(int bumpActive){
		
		
        
        if(bumpActive == 1){
        	
        	try{
	        	
				SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefEditor = loginSettings.edit();  
				prefEditor.putInt("bump", 0); 
				prefEditor.putInt("reciever", 0); 
				prefEditor.commit(); 
				
				MyAppData appState = ((MyAppData)getApplicationContext());
				appState.destroy(bumpActive);
				//getApplicationContext().unbindService(cardConnection);
				
				//getApplicationContext().unregisterReceiver(cardReceiver);
				System.out.println("service destroyed");
				
	       	
	       }catch(Exception e){
	    	   
	    		Log.d("BumpTest DigCard","excepton =="+e);
	       	
	       	Log.d("BumpTest DigCard","unbound service crash");

	       	
	       }
        }
		
	}
	*/
	
	public void doSearch(String string){
		
		
		if(string.length()!=0){
			
			
			
			lowSearch=string;
			
			fillData();
			
		}else{
			
			lowSearch=null;
			fillData();
		}
		
	}
	
	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_delete_menu, menu);
		
		//return super.onCreateOptionsMenu(menu);
		return true;
	}

	
	
	
	
	
	@Override
	public boolean onPrepareOptionsMenu (Menu menu) {
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int down = loginSettings.getInt("delete", 0);
        int trans= loginSettings.getInt("transfer", 0);
		
	    if (down == 1){
	    	menu.getItem(0).setVisible(false);
	    	menu.getItem(1).setVisible(false);
	    	menu.getItem(2).setVisible(false);
	    	menu.getItem(3).setVisible(false);
	    	menu.getItem(4).setVisible(true);
	    	menu.getItem(5).setVisible(true);
	    }else if(trans ==1){
	    	menu.getItem(0).setVisible(false);
	    	menu.getItem(1).setVisible(false);
	    	menu.getItem(2).setVisible(false);
	    	menu.getItem(3).setVisible(false);
	    	menu.getItem(4).setVisible(true);
	    	menu.getItem(5).setVisible(false);
	    	
	    }else{
	    	menu.getItem(0).setVisible(true);
	    	menu.getItem(1).setVisible(true);
	    	menu.getItem(2).setVisible(true);
	    	menu.getItem(3).setVisible(false);
	    	menu.getItem(4).setVisible(false);
	    	menu.getItem(5).setVisible(false);
	    }  
	    return true;
	}*/
	
	
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		
		switch(item.getItemId()){
			
			case R.id.menu_delete:
				
				delete();
				break;
				
			case R.id.menu_transfer:
				
				transfer();
				break;
				
			case R.id.menu_delete_all:
				
				deleteAllAlert();
				break;	
				
			/*case R.id.menu_bump:
				
				Toast.makeText(NewCardList.this, "Bump to transfer the Card", Toast.LENGTH_LONG).show();
				insertRecievedData("name");
				break;	
			*/	
			case R.id.menu_bump_cancel:
				
				bumpCancelAlert();
				break;
				
			case R.id.menu_delete_button:
				
				delete_alert();
				break;	
		}
		return super.onMenuItemSelected(featureId, item);
		
		
	}
	
	
	
	public void bumpCancelAlert(){
		
		
		
        
		  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = loginSettings.edit();  
			prefEditor.putInt("transfer", 0); 
			prefEditor.putInt("delete", 0); 
			prefEditor.commit();  
			
			
			String data=searchMyContactText();
			doSearch(data);
			
			//fillData();
		  
		  
	  	
	
	
}
	
	public void deleteAllAlert(){
		
		this.listView=getListView();
		int count = listView.getCount();
		if(count ==0){
			AlertDialog.Builder builder = new AlertDialog.Builder(NewCardList.this);
			builder.setCancelable(false);
			
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			       
				  
				  
			  }});
			
			
			
			
			builder.setMessage("No Cards").create().show();
		}else{
			AlertDialog.Builder builder = new AlertDialog.Builder(NewCardList.this);
			builder.setTitle("Delete All");
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			       
				  ProgressDialog loadingDialog = new ProgressDialog(NewCardList.this);
		            loadingDialog.setMessage("Deleting Cards..");
		            loadingDialog.setCancelable(false);
		            loadingDialog.show();
				  Uri deleteAllUri=CardManagingProvider.CONTENT_URI_DETAIL;
				 
				  resolver.delete(deleteAllUri, null, null);
				  fillData();
				  try{
						
						loadingDialog.dismiss();
						loadingDialog=null;
						
					}catch(Exception e){
						
						//System.out.println("loadingDialog exception"+e);
					}
				  //Toast.makeText(NewCardList.this, "Cards Deleted Successfully", Toast.LENGTH_SHORT).show();
				  alertFunction("Deleted Successfully");
				  
			  }});
			
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					  dialog.dismiss();
					  
					  
				        
					  
				  }});
			
			
			
			builder.setMessage("Do you want to delete all cards?").create().show();
		}
		
		
		
	}

	
	public void transfer(){
		
		int number=0;
		this.listView=getListView();
		int count = listView.getCount();
		
		
		if(count==0){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(NewCardList.this);
			builder.setTitle("Transfer");
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			        
				  		
				  
				  
			  }});
			
			
			
			
			
			builder.setMessage("No Cards Available for Transferring").create().show();
			
			
		}else{
			
			
			
			AlertDialog.Builder builder = new AlertDialog.Builder(NewCardList.this);
			builder.setTitle("Transfer");
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			        
				  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor prefEditor = loginSettings.edit();  
					prefEditor.putInt("transfer", 1); 
					prefEditor.putInt("delete", 0); 
					prefEditor.commit();
					/*try {
						
						if(api!=null){
							
							api.simulateBump();
							
						}
						
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					//fillData();
					
					String data=searchMyContactText();
					doSearch(data);
					
				  
				  
			  }});
			
			
			
			
			
			builder.setMessage("Please select the required cards and bump to transfer").create().show();
			
			
		}
		
		
	
		
		
		
	}
	
	
	public void delete(){
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("delete", 1);
		prefEditor.putInt("transfer", 0); 
		prefEditor.commit();
		
		String searchText=searchMyContactText();
		
		doSearch(searchText);
		
		
		//fillData();
		
	}
	
	
	public void transfer_alert(){
		
		
					
			AlertDialog.Builder builder = new AlertDialog.Builder(NewCardList.this);
			builder.setTitle("Cancel");
			builder.setCancelable(false);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			        
				  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor prefEditor = loginSettings.edit();  
					prefEditor.putInt("transfer", 0); 
					prefEditor.commit();  
					
					String data=searchMyContactText();
					doSearch(data);
					//fillData();
				  
				  
			  }});
			
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					  dialog.dismiss();
					  
					  
						
						
				        
					  
				  }});
			
			
			
			builder.setMessage("You want  to cancel card transfer").create().show();
		
		
		
	}
	
	
	public void delete_alert(){
		
		
		int number=0;
		this.listView=getListView();
		int count = listView.getCount();
		
		for(int j=0;j<count;j++){
			
			
			boolean data=tick.get(j);
			
			if(data){
				
				number++;
				
	
			}
		}
		
		if(number==0){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(NewCardList.this);
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			        
				  
			  }});
			
			/*builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					  dialog.dismiss();
					  
					  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
						SharedPreferences.Editor prefEditor = loginSettings.edit();  
						prefEditor.putInt("delete", 0); 
						prefEditor.commit();  
						
						String data=searchMyContactText();
						doSearch(data);
						//fillData();
						
						
					  
				  }});*/
			
			
			builder.setTitle("Warning");
			builder.setMessage("Please Select atleast one card").create().show();
		}else {
			
			itemDeleteAlert();
		}
		
	}
	
	
	public void itemDeleteAlert(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(NewCardList.this);
		builder.setTitle("Delete");
		builder.setCancelable(false);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface arg0, int arg1) {
		  // do something when the OK button is clicked
			  
			 itemDelete();
		        
			  
		  }});
		
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
				  
				  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor prefEditor = loginSettings.edit();  
					prefEditor.putInt("delete", 0); 
					prefEditor.commit();  
					
					String data=searchMyContactText();
					doSearch(data);
					//fillData();
			        
				  
			  }});
		
		//builder.show();
		builder.setMessage("Do You want to delete ?").create().show();
		
	}
	
	
	public void onResume(){
		super.onResume();
		
		
		boolean tablet= isTablet(this);
		if(tablet){
			
			MainActivity.getInstance().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}else{
			
			MainActivity.getInstance().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			
		}
		
		MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	parent.manipulateTitle("Digcard");
		
		SharedPreferences itemSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = itemSettings.edit();  
		prefEditor.putInt("number", 0); 
		//prefEditor.putInt("save", 0); 
		prefEditor.commit(); 
		
		bumpServiceStart();
		
		
		
		
		//fillData();
		String data=searchMyContactText();
		doSearch(data);
		
	}
	
	
	public boolean isTablet(Context context) {  
        return (context.getResources().getConfiguration().screenLayout   
                & Configuration.SCREENLAYOUT_SIZE_MASK)    
                >= Configuration.SCREENLAYOUT_SIZE_LARGE; 
	}
	
	
	public void bumpServiceStart(){
		
		serviceCount++;
		
		SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        int recieverActive=deleteSettings.getInt("reciever", 0);
        
        
        
        if(bumpActive==0){
        	
        	
        	
        	MainActivity parent;
        	parent =(MainActivity) this.getParent();
        	parent.bumpServiceStart();
        		
        }/*else{
        	
        	int value=0;
        	MainActivity parent;
        	parent =(MainActivity) this.getParent();
        	parent.manipulateBump(value);
        	
        }*/	
	}
	

	
	public void fillData() {
		// TODO Auto-generated method stub
		
		
		
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("number", 0); 
		prefEditor.commit(); 
		
		
		//SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
      //  int display= deleteSettings.getInt("delete", 0);
       // int show=loginSettings.getInt("transfer", 0);
        
        String[] projection=new String[] {CardTable.KEY_ROWID, CardTable.KEY_NAME, CardTable.KEY_DESIGNATION, CardTable.KEY_COMPANY, CardTable.KEY_NUMBER, CardTable.KEY_PROFILE_PIC};
        Uri uri=CardManagingProvider.CONTENT_URI_DETAIL;
        
        
        
        if(lowSearch ==null){
        	
        	Cursor sizeCurser=getContentResolver().query(uri,new String[]{CardTable.KEY_ROWID}, null, null, null );
            

            for (int i = 0; i < sizeCurser.getCount(); i++) {
               
            	tick.add(i, false);
                
            }
            
            sizeCurser.close();
            //System.out.println("sizeCurser =="+sizeCurser.getCount());
            
            
            double number =(double) sizeCurser.getCount()/100;
            //System.out.println("float number =="+ number);
            int iteration=(int) Math.ceil(number);
            //System.out.println("iteration =="+iteration);
            int offset=0;
            
            
            
        	myCardList=new ArrayList<HashMap<String, String>>();
        	myImageList   = new ArrayList<HashMap<String, byte[]>>();
        	
		        	for(int j=0;j<iteration;j++){
		        		
		        		String sortOrder = String.format("%s limit "+offset+",100",CardTable.KEY_ROWID);
		            	cursor = getContentResolver().query(uri, projection, null, null, sortOrder );
		            	offset+=100;
		            	
		            	
		            	if ( cursor != null && cursor.getCount() > 0 ) {
		            		
		            		
		    	 		    if (cursor.moveToFirst()) {
		    	 		        // loop until it reach the end of the cursor
		    	 		        do {
		    	 		        	HashMap<String, String> map=new HashMap<String, String>();
		    	 		        	HashMap<String, byte[]> imageMap=new HashMap<String, byte[]>();
		    	 		        	
		    	 		        	String row_id=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_ROWID));
		    	 		        	String name=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_NAME));
		    	 		        	String phone=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_NUMBER));
		    	 		        	String designation=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_DESIGNATION));
		    	 		        	String company=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_COMPANY));
		    	 		        	byte[] profile_pic=cursor.getBlob(cursor.getColumnIndexOrThrow(CardTable.KEY_PROFILE_PIC));
		    	 					
		    	 		        	
		    	 					map.put("row_id", row_id);
		    			        	map.put("name", name);
		    			        	map.put("designation", designation);
		    			        	map.put("company", company);
		    			        	map.put("phone", phone);
		    			        	
		    			        	imageMap.put("pic", profile_pic);
		    			        	
		    			        	myCardList.add(map);
		    			        	myImageList.add(imageMap);
		    			        	
		    	 		        } while (cursor.moveToNext());
		    	 		    }
		
		    	 		   
		    	 		           	
		            	}
		            	
		            	 // make sure to close the cursor
	    	 		    cursor.close();
		        }
        	
        }else{
        	
        	
        	
        	String select=CardTable.KEY_NAME +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_COMPANY +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_DESIGNATION +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_NUMBER +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_ADDRESS +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_EMAIL +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_DESCRIPTION +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_COMMENTS +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_WEB_ADDRESS +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_FAX +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_OFFICE +" LIKE "+"'%"+lowSearch+"%'";
		        	
        	Cursor sizeCurser=getContentResolver().query(uri,new String[]{CardTable.KEY_ROWID}, select, null, null );
        	
        	
        	for (int i = 0; i < sizeCurser.getCount(); i++) {
                
            	tick.add(i, false);
                
            }
            
            sizeCurser.close();
            
            
            double number =(double) sizeCurser.getCount()/100;
            
            int iteration=(int) Math.ceil(number);
            int offset=0;
            
            
        	myCardList=new ArrayList<HashMap<String, String>>();
        	myImageList   = new ArrayList<HashMap<String, byte[]>>();
        	
		        	for(int j=0;j<iteration;j++){
		        		
		        		String sortOrder = String.format("%s limit "+offset+",100",CardTable.KEY_ROWID);
		            	//cursor = getContentResolver().query(uri, projection, CardTable.KEY_NAME +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_COMPANY +" LIKE "+"'%"+lowSearch+"%'", null, sortOrder);
		        		String selection=CardTable.KEY_NAME +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_COMPANY +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_DESIGNATION +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_NUMBER +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_ADDRESS +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_EMAIL +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_DESCRIPTION +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_COMMENTS +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_WEB_ADDRESS +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_FAX +" LIKE "+"'%"+lowSearch+"%'"+" OR " + CardTable.KEY_OFFICE +" LIKE "+"'%"+lowSearch+"%'";
		        		
		        		
		        		cursor = getContentResolver().query(uri, projection, selection, null, sortOrder);
		        		offset+=100;
		            	
		            	
		            	if ( cursor != null && cursor.getCount() > 0 ) {
		            		
		            		
		    	 		    if (cursor.moveToFirst()) {
		    	 		        // loop until it reach the end of the cursor 
		    	 		        do {
		    	 		        	HashMap<String, String> map=new HashMap<String, String>();
		    	 		        	HashMap<String, byte[]> imageMap=new HashMap<String, byte[]>();
		    	 		        	
		    	 		        	String row_id=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_ROWID));
		    	 		        	String name=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_NAME));
		    	 		        	String phone=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_NUMBER));
		    	 		        	String designation=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_DESIGNATION));
		    	 		        	String company=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_COMPANY));
		    	 		        	byte[] profile_pic=cursor.getBlob(cursor.getColumnIndexOrThrow(CardTable.KEY_PROFILE_PIC));
		    	 					
		    	 		        	
		    	 					map.put("row_id", row_id);
		    			        	map.put("name", name);
		    			        	map.put("designation", designation);
		    			        	map.put("company", company);
		    			        	map.put("phone", phone);
		    			        	
		    			        	imageMap.put("pic", profile_pic);
		    			        	
		    			        	myCardList.add(map);
		    			        	myImageList.add(imageMap);
		    			        	
		    	 		        } while (cursor.moveToNext());
		    	 		    }
		
		    	 		   
		    	 		    
		    	 		           	
		            	}
		            	 // make sure to close the cursor
		            	cursor.close();
		        }
        	
        }
        
        
       /* startManagingCursor(cursor); 
		String[] from = new String[] { CardTable.KEY_ROWID, CardTable.KEY_NAME, CardTable.KEY_DESIGNATION, CardTable.KEY_COMPANY, CardTable.KEY_NUMBER };
		int[] to = new int[] { R.id.local_id, R.id.local_name, R.id.local_designation,R.id.local_company, R.id.local_phone };
		
		
		mcAdapter = new MyCursorAdapter(NewCardList.this, R.layout.local_list, cursor, from, to);
		 
		setListAdapter(mcAdapter);*/
        
        
        mcAdapter = new MyCustomAdapter(NewCardList.this,myCardList,myImageList);
		 
		 setListAdapter(mcAdapter);
		 
	}
	
	@Override
    public void onDestroy() {
       
		/*		     
        SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
        int bumpActive= deleteSettings.getInt("bump", 0);
        
        
        MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	parent.bumpServiceCancel(bumpActive);       
        */
       super.onDestroy(); 
    }
	
	
	public void onPause(){
		super.onPause();
		MainActivity.getInstance().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		try{
			
			//cursor.close();
			
		}catch(Exception e){
			
			
		}
		
	}
	
	private void itemDelete(){
		
		this.listView=getListView();
		int count = listView.getCount();
		
		
		 ProgressDialog loadingDialog = new ProgressDialog(this);
         loadingDialog.setMessage("Deleting..");
         loadingDialog.setCancelable(false);
         loadingDialog.show();
		
		for(int j=0;j<count;j++){
			
			boolean data=tick.get(j);
			
			if(data){
				
				HashMap<String, String> map = myCardList.get(j);
				String val=map.get("row_id");
				
				
								
				
				
				Uri cardUri=Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL_LOCAL + "/" + val);
				
				resolver.delete(cardUri, null, null);
				
			}
			
		}
		
		try{
			
			loadingDialog.dismiss();
			loadingDialog=null;
			
		}catch(Exception e){
			
			
		}
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("delete", 0); 
		prefEditor.commit();  
		
		fillData();
		
		//Toast.makeText(NewCardList.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
		
		alertFunction("Deleted Successfully");
		
		
	}

public class MyCustomAdapter extends BaseAdapter implements View.OnClickListener {
	
	
	ArrayList<HashMap<String, String>> list = null;
	ArrayList<HashMap<String, byte[]>>  imageList=null;
	Context myContext;
	private LayoutInflater mInflater;
	private Bitmap mIcon1;
	private int value,bluetooth;

	public MyCustomAdapter(Context context, ArrayList<HashMap<String, String>> myCardList, ArrayList<HashMap<String, byte[]>> myImageList) {
		// TODO Auto-generated constructor stub
		
		
		this.myContext = context;
		this.list = myCardList;
		this.imageList = myImageList;
		this.mInflater= LayoutInflater.from(context); 
		mIcon1=BitmapFactory.decodeResource(context.getResources(), R.drawable.profile_pic);
		
		 SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
         value = loginSettings.getInt("delete", 0);
         bluetooth=loginSettings.getInt("transfer", 0);
         
         for (int i = 0; i < this.list.size(); i++) {
        	 
             tick.add(i, false);
             
         }
	}

	
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) { 
			
			convertView = mInflater.inflate(R.layout.local_list, null);
			holder = new ViewHolder(); 
			holder = new ViewHolder();
			 
			 holder.hold_name = (TextView) convertView.findViewById(R.id.local_name); 
			 holder.hold_designation = (TextView) convertView.findViewById(R.id.local_designation); 
			 holder.hold_company = (TextView) convertView.findViewById(R.id.local_company); 
			 holder.hold_number = (TextView) convertView.findViewById(R.id.local_phone); 
			 holder.hold_pic = (ImageView) convertView.findViewById(R.id.local_imag); 
			 holder.hold_check = (CheckBox) convertView.findViewById(R.id.local_id);
			convertView.setTag(holder); 
			
		}else { 
			holder = (ViewHolder) convertView.getTag(); 
		} 
		
		HashMap<String, String> map = this.list.get(position);
		HashMap<String, byte[]> imageMap = this.imageList.get(position);
		
		if(value ==1 ||bluetooth ==1){
			
			holder.hold_check.setVisibility(View.VISIBLE);
		}
		
		/*map.put("row_id", row_id);
    	map.put("name", name);
    	map.put("designation", designation);
    	map.put("company", company);
    	map.put("phone", phone);
		*/
		
		String l_name = map.get("name").toString();
	    String l_number = map.get("phone").toString();
	    String l_company = map.get("company").toString();
	    String l_designation = map.get("designation").toString();
	   // String l_id = map.get("row_id").toString();
	    byte[] imageAsBytes= imageMap.get("pic");
	    
	    
	    holder.hold_name.setText(l_name);
	    holder.hold_designation.setText(l_designation);
	    holder.hold_company.setText(l_company);
	    holder.hold_number.setText(l_number);
	    
	    
	    if(imageAsBytes !=null){
			
			holder.hold_pic.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
			
		}else{
			
			holder.hold_pic.setImageBitmap(mIcon1);
		}
	    
	    
	    holder.hold_check.setTag(Integer.valueOf(position));
	    holder.hold_check.setOnClickListener(this);
	    holder.hold_check.setChecked(tick.get(position));
		
		return convertView;
	}

	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		

		Integer index = (Integer)v.getTag();
	     boolean state = tick.get(index.intValue());

	     tick.set(index.intValue(), !state);
	}
	
	
	private final class ViewHolder {
		public TextView hold_name;
        public TextView hold_designation;
        public TextView hold_company;
        public TextView hold_number;
        public ImageView hold_pic;
        public CheckBox  hold_check;
	}
	
}




/*
	
public class MyCursorAdapter extends SimpleCursorAdapter implements View.OnClickListener {
	
	
	private final Context mContext;
	private final int mLayout;
	private final Cursor mCursor;
	private final int mNameIndex;
	private final int mNumberIndex;
	private final int mCompanyIndex;
	private final int mDesigIndex;
	private final int mPicIndex;
	private final LayoutInflater mLayoutInflater;
	private int value,bluetooth;
	private Bitmap mIcon1;
	

	public MyCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
		super(context, layout, c, from, to);
		// TODO Auto-generated constructor stub
		this.mContext = context;
	    this.mLayout = layout;
	    this.mCursor = c;
	    this.mLayoutInflater = LayoutInflater.from(mContext);
	    this.mNameIndex = mCursor.getColumnIndexOrThrow(CardTable.KEY_NAME);
	    this.mNumberIndex = mCursor.getColumnIndexOrThrow(CardTable.KEY_NUMBER);
	    this.mDesigIndex = mCursor.getColumnIndexOrThrow(CardTable.KEY_DESIGNATION);
	    this.mPicIndex = mCursor.getColumnIndexOrThrow(CardTable.KEY_PROFILE_PIC);
	    this.mCompanyIndex = mCursor.getColumnIndexOrThrow(CardTable.KEY_COMPANY);
	    
	    mIcon1=BitmapFactory.decodeResource(context.getResources(), R.drawable.profile_pic);
	    
	    SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
         value = loginSettings.getInt("delete", 0);
         bluetooth=loginSettings.getInt("transfer", 0);
         
         for (int i = 0; i < c.getCount(); i++) {
             tick.add(i, false);
         }
	    

		
	}
	
	
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(mCursor.getCount() > 0){
			
			
			if (mCursor.moveToPosition(position)) {
				ViewHolder holder; 
				
				if (convertView == null) {
				
					 convertView = mLayoutInflater.inflate(mLayout, null);
					 //convertView = mLayoutInflater.inflate(R.layout.local_list, null);

					 holder = new ViewHolder();
					 
					 holder.hold_name = (TextView) convertView.findViewById(R.id.local_name); 
						holder.hold_designation = (TextView) convertView.findViewById(R.id.local_designation); 
						holder.hold_company = (TextView) convertView.findViewById(R.id.local_company); 
						holder.hold_number = (TextView) convertView.findViewById(R.id.local_phone); 
						holder.hold_pic = (ImageView) convertView.findViewById(R.id.local_imag); 
						holder.hold_check = (CheckBox) convertView.findViewById(R.id.local_id);
						
						convertView.setTag(holder);

				}else {
					
					holder = (ViewHolder) convertView.getTag();

				}
				
				
				if(value ==1 ||bluetooth ==1){
					
					holder.hold_check.setVisibility(View.VISIBLE);
				}
				
				
				String data_name=mCursor.getString(mNameIndex);
				String data_number=mCursor.getString(mNumberIndex);
				String data_designation=mCursor.getString(mDesigIndex);
				String data_company=mCursor.getString(mCompanyIndex);
				byte[] data_pic=mCursor.getBlob(mPicIndex);
				
				
				holder.hold_name.setText(data_name);
			    holder.hold_designation.setText(data_designation);
			    holder.hold_company.setText(data_company);
			    holder.hold_number.setText(data_number);
				
			    
			    if(data_pic !=null){
					
					byte[] imageAsBytes;
					//imageAsBytes = Base64.decode(data_pic.getBytes());
					imageAsBytes =data_pic;
					holder.hold_pic.setImageBitmap(
					        BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
				}else{
					
					holder.hold_pic.setImageBitmap(mIcon1);
				}
			    
			    
			    holder.hold_check.setTag(Integer.valueOf(position));
			    holder.hold_check.setOnClickListener(this);
			    holder.hold_check.setChecked(tick.get(position));
			}
			
		}
		

		
		
		return convertView;
		
		
	}
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		
		Integer index = (Integer)v.getTag();
	     boolean state = tick.get(index.intValue());

	     tick.set(index.intValue(), !state);
		
	}
	
	
	private final class ViewHolder {
		public TextView hold_name;
        public TextView hold_designation;
        public TextView hold_company;
        public TextView hold_number;
        public ImageView hold_pic;
        public CheckBox  hold_check;
	}

	
	
}
*/

	protected void onListItemClick(ListView l, View v, int position, long id){
		super.onListItemClick(l, v, position, id);
	
	
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	    int display= loginSettings.getInt("delete", 0);
	    int send   = loginSettings.getInt("transfer", 0);
	    
	    if(display == 1 || send==1){
	    	
	    	boolean currentlyChecked = tick.get(position);
	    	
	    	tick.set(position, !currentlyChecked);
	    	CheckBox markedItem = (CheckBox) v.findViewById(R.id.local_id);
			markedItem.setChecked(tick.get(position));
			
	    	
	    }else{
	    	
	    	HashMap<String, String> map = myCardList.get(position);
			String val=map.get("row_id");
		
			//System.out.println(l.getItemAtPosition(position));
			//System.out.println("id ="+id);
			SharedPreferences autoSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        	SharedPreferences.Editor prefAutoEditor = autoSettings.edit();
        	prefAutoEditor.putInt("dntReset", 1);
        	prefAutoEditor.commit();
			
			
			l.getItemAtPosition(position);
			Intent edit_intent = new Intent(NewCardList.this, LocalCard.class);
			//Uri listUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL_LOCAL + "/" + id);
			Uri listUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL_LOCAL + "/" + val);
			edit_intent.putExtra(CardManagingProvider.CONTENT_ITEM_TYPE, listUri);
			
			startActivity(edit_intent);
	    }
	}

	
	
	
    
    public String sendData(){
    	
		int select=0;
    	
    	String transferArrayData=null;
    	
    	JSONArray ja = new JSONArray();
    	
    	   	
    	this.listView=getListView();
		int count = listView.getCount();
		
		SharedPreferences transferSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
		 int show=transferSettings.getInt("transfer", 0);
		 
		 if(show ==1){
			 
			 
			 for(int j=0;j<count;j++){
					
					boolean data=tick.get(j);
					
					if(data){
						
						select++;
						
						//long val =listView.getItemIdAtPosition(j);
						
						HashMap<String, String> map = myCardList.get(j);
						String val=map.get("row_id");
						
						
						//Toast.makeText(NewCardList.this, "row_id =="+val, Toast.LENGTH_LONG).show();
						
						Uri cardUri=Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL_LOCAL + "/" + val);
						
						
						Cursor cursor = getContentResolver().query(cardUri, null, null, null, null);
						if (cursor != null) {
							if(cursor.moveToFirst()){
								
								
								String id=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_USERID));
								
								String name=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_NAME));
								String number=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_NUMBER));
								String designation=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_DESIGNATION));
								String company=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_COMPANY));
								String address=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_ADDRESS));
								String email=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_EMAIL));
								String updateDate=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_UPDATE_DATE));
								String description=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_DESCRIPTION));
								String comment=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_COMMENTS));
								String webUrl=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_WEB_ADDRESS));
								String video=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_VIDEO));
								String fax=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_FAX));
								String office=cursor.getString(cursor
										.getColumnIndexOrThrow(CardTable.KEY_OFFICE));
								
								
								
								JSONObject obj = new JSONObject();
						       	 try {
						                obj.put("user_id", id);
						                obj.put("name", name);
						                obj.put("email", email);
						                obj.put("phone", number);
						                obj.put("companyname", company);
						                obj.put("designation", designation);
						                obj.put("companyaddress", address);
						                obj.put("dateofupdate", updateDate);
						                obj.put("description", description);
						                obj.put("comment", comment);
						                obj.put("weburl", webUrl);
						                obj.put("video", video);
						                obj.put("office", office);
						                obj.put("fax", fax);
						                
						              
						                
						                ja.put(obj);
						                
						                
						                
						       	 } catch (JSONException e) {
						                
						       		 Log.w("BumpTest", e);
						            }
								
							}
						}
						
						cursor.close();
						
					}
					
					
					
				}
			 
		 }
		
		
		
		if(select ==0){
			
			return null;
			
		}else{
			
			transferArrayData=ja.toString();
			
	    	return transferArrayData;
			
		}
		
		
    	
    }
    
    public void alertFunction(String message){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
 
    	
}



