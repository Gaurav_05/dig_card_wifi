package digcard_wifi.app.com;



import java.io.File;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;





public class MyAppData extends Application{
	
	public Object appData=null;
	public Object appCardlistData=null;
	public Object serviceConnect=null;
	public Object recieverObject=null;
	public Object api=null;
	public String count=null;
	public int number=0;
	public int tab=0;
	public byte[] array=null;
	public byte[] profileArray=null;
	
	public void onCreate(){
		
		boolean connection=haveNetworkConnection();
		
		if(connection){
			
			 //getApplicationContext().startService(new Intent(IBumpAPI.class.getName()));
			//Toast.makeText(MyAppData.this, "bump service started", Toast.LENGTH_LONG).show();
		}
		
		
	}
	
	public void setState(Object recievdData){
		
		//System.out.println("in my app data to set data");
		appData = recievdData;
		
	  }
	
	public void setCardlistState(Object recievdCardlistData){
		
		//System.out.println("in my app data to set data");
		appCardlistData = recievdCardlistData;
		
	  }
	
	
	public void setServiceConnect(Object connection){
		
		serviceConnect=connection;
	}
	
	public void setReciever(Object reciever){
		
		recieverObject=reciever;
	}
	
	public void setCount(String row_id){
		
		count=row_id;
		
	}
	
	public void setCurrentItem(int item){
		
		number=item;
	}
	
	public void tablet(int value){
		
		tab=value;
	}
	
	public int getTablet(){
		
		return tab;
	}
	
	public Object getState(){
		
		//System.out.println("in my app data to get data");
		
	    return appData;
	    
	  }
	public Object getCardlistState(){
		
		//System.out.println("in my app data to get data");
		
	    return appCardlistData;
	    
	  }
	
	public Object getServiceConnect(){
		
		//System.out.println("in my app data to get data");
		
	    return serviceConnect;
	    
	  }
	
	public Object getReciever(){
		
		//System.out.println("in my app data to get data");
		
	    return recieverObject;
	    
	  }
	 
	private boolean haveNetworkConnection() {
	    boolean haveConnectedWifi = false;
	    boolean haveConnectedMobile = false;

	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	            if (ni.isConnected())
	                haveConnectedWifi = true;
	        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	            if (ni.isConnected())
	                haveConnectedMobile = true;
	    }
	    return haveConnectedWifi || haveConnectedMobile;
	}
	
	
	public String getCount(){
		return count;
		
		
	}
	public int getCurrentItem(){
		return number;
		
		
	}


	public void setImageByteArray(byte[] byteArray) {
		
		array=byteArray;
		
	}
	
	public byte[] getImageByteArray(){
		
		
		return array;
	}
	
	
	public void setProfileImageByteArray(byte[] profileByteArray) {
		
		profileArray=profileByteArray;
		
	}
	
	public byte[] getProfileImageByteArray(){
		
		
		return profileArray;
	}
	
	public void clearImageByteArray(){
		
		array=null;
		profileArray=null;
	}
	
	public void clearApplicationData() {
		//File cache = getCacheDir();
		
		try {
		       File dir = this.getCacheDir();
		       if (dir != null && dir.isDirectory()) {
		          deleteDir(dir);
		          Log.i("TAG", "**************** File /data/data/APP_PACKAGE/cache DELETED *******************");
		       }
		    } catch (Exception e) {
		       // TODO: handle exception
		    	
		    	Log.i("TAG", "**************** File /data/data/APP_PACKAGE/cache not DELETED *******************");
		    }
		
		/*File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
				for (String s : children) {
					if (!s.equals("lib")) {
						deleteDir(new File(appDir, s));
						Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
					}
				}
		}*/
	}

			
	  public static boolean deleteDir(File dir) {
			    if (dir != null && dir.isDirectory()) {
			       String[] children = dir.list();
			       for (int i = 0; i < children.length; i++) {
			          boolean success = deleteDir(new File(dir, children[i]));
			          if (!success) {
			             return false;
			          }
			       }
			    }

			    // The directory is now empty so delete it
			    return dir.delete();
			 }
	  
	  public void setBumpApi(Object bumpApi){
		  api = bumpApi;
	  }
	  
	  public Object getBumpApi(){
		return api;
		  
	  }
}
