package digcard_wifi.app.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

public class Logout extends Activity{
	
	
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.logout);
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("number", 5); 
		prefEditor.commit(); 
		//show_alert();
		 
	}
	
	
	
	
	public void show_alert(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(Logout.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface arg0, int arg1) {
		  // do something when the OK button is clicked
			  
			  SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			  int auto=checkSettings.getInt("auto", 0);
			  
			  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
		        
		        SharedPreferences.Editor prefEditor = loginSettings.edit();  
		        prefEditor.putString("user_name", null);  
		        prefEditor.putString("password", null);
		        prefEditor.putString("user_id", null);
		        prefEditor.putInt("out", 1);
		       // prefEditor.putInt("number", 3);
		        prefEditor.putInt("login", 0); 
		        prefEditor.putString("register", null); 
				prefEditor.commit(); 
			  
		        SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
        	    int bumpActive= deleteSettings.getInt("bump", 0);
        	    
        	   /* if(bumpActive == 1){
        	    	
        	    	MainActivity parent;
        	    	parent =(MainActivity) Logout.this.getParent();
        	    	parent.bumpServiceCancel(bumpActive);
        	    	
        	    }
		        
		        Intent intnte= new Intent(Logout.this, MainActivity.class);
		        intnte.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intnte);
		        */
        	    
        	   // MyAppData appState = ((MyAppData)getApplication());
        	   // appState.clearImaheByteArray
        	    
        	    MainActivity parent;
    	    	parent =(MainActivity) Logout.this.getParent();
    	    	parent.changeTab();
			  
		  }});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		       
		  public void onClick(DialogInterface arg0, int arg1) {
		  // do something when the Cancel button is clicked
			  
			  switchTabInActivity(1);
			  
		  }});
		//builder.show();
		builder.setTitle("Logout");
		builder.setMessage("Are you sure want to logout ?").create().show();
		
	}
	
	
	
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		
		switch(item.getItemId()){
			
			case R.id.menu_exit:
				
				MainActivity parent;
    	    	parent =(MainActivity) Logout.this.getParent();
    	    	parent.exitApplication();
				break;
				
			
		}
		return super.onMenuItemSelected(featureId, item);
		
		
	}
	
	
	private void switchTabInActivity(int i) {
		// TODO Auto-generated method stub
    	
    	MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	parent.switchTab(i);
	}

	public void onResume(){
		super.onResume();
		
		
		
		MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	parent.manipulateTitle("Logout");
		
		SharedPreferences itemSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = itemSettings.edit();  
		prefEditor.putInt("number", 5); 
		prefEditor.commit(); 
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		int save=loginSettings.getInt("save", 0);
		
		if(save!=1){
			
			show_alert();
			
		}else{
			
			SharedPreferences settings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor editor = settings.edit();  
			editor.putInt("save", 0); 
			editor.commit();
		}
		
	}
	
	
	@Override
	public void onBackPressed() {
		
		
		SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
	    int bumpActive= deleteSettings.getInt("bump", 0);
	    final int auto= deleteSettings.getInt("auto", 0);
	    /*if(bumpActive == 1){
	    	
	    	MainActivity parent;
	    	parent =(MainActivity) this.getParent();
	    	parent.bumpServiceCancel(bumpActive);
	    	
	    }*/
		
		
		//super.onBackPressed();
		AlertDialog.Builder builder = new AlertDialog.Builder(Logout.this);
		builder.setTitle("Exit");
		builder.setCancelable(false);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			  
			  dialog.dismiss();
			  
			  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefEditor = loginSettings.edit();  
				
				//if(auto==0){
					prefEditor.putString("user_name", null);  
				    prefEditor.putString("password", null);
				    prefEditor.putString("user_id", null);
					prefEditor.putInt("number", 3);
					
				/*}else{
					
					prefEditor.putInt("number", 4);
					
				}*/
				
				prefEditor.commit();
		        
			 finish();
			  
		  }});
		
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
				  
				  
					   return;
					
					
					
			        
				  
			  }});
		
		
		
		builder.setMessage("You want to exit application").create().show();
	}

}
